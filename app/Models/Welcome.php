<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Welcome extends Model
{
	use SoftDeletes;

    protected $table = "welcome";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];
}
