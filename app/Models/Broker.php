<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Broker extends Model
{
	use SoftDeletes;

    protected $table = "brokers";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];
}
