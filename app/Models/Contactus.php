<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contactus extends Model
{
	use SoftDeletes;

    protected $table = "contactus";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    public function countries()
    {
    	return $this->hasOne('App\Models\Country','id','country');
    }

    public function homesize()
    {
    	return $this->hasOne('App\Models\Homesize','id','home_size');
    }

    public function brokers()
    {
    	return $this->hasOne('App\Models\Broker','id','broker');
    }

    public function hearabout()
    {
    	return $this->hasOne('App\Models\Reference','id','hearaboutus');
    }
}
