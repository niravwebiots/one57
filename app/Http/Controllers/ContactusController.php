<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contactus;
use Mail;

class ContactusController extends Controller
{
    public function index()
    {
        $contactus = Contactus::with('countries')->with('homesize')->with('brokers')->with('hearabout')->get();
        return view('admin.contactus.index',compact('contactus'));
    }

    public function create(Request $request)
    {
        $this->validate($request,[
            'your_name' => 'required',
            'your_email' => 'required|email',
            'your_phone' => 'required|numeric',
            'your_address' => 'required',
            'your_country' => 'required',
            'your_city' => 'required',
            'your_state' => 'required',
            'your_zip' => 'required',
            'your_homesize' => 'required',
            'your_broker' => 'required',
            'your_reference' => 'required'
        ]);

        $contact = New Contactus;
        $contact->customer_name = $request->your_name;
        $contact->email = $request->your_email;
        $contact->contactno = $request->your_phone;
        $contact->address = $request->your_address;
        $contact->country = $request->your_country;
        $contact->city  = $request->your_city;
        $contact->state = $request->your_state;
        $contact->zip = $request->your_zip;
        $contact->home_size = $request->your_homesize;
        $contact->broker = $request->your_broker;
        $contact->hearaboutus = $request->your_reference;
        $contact->status = 1;

        if($contact->save())
        {
            /*Mail::send('email.page', ['contact' => $contact], function ($m) use ($contact) {
                $m->from('hello@app.com', 'Your Application');

                $m->to($contact->email, $contact->name)->subject('Your Reminder!');
            });

            Mail::send('email.page', ['contact' => $contact], function ($m) use ($contact) {
                $m->from('hello@app.com', 'Your Application');

                $m->to($contact->email, $contact->name)->subject('Your Reminder!');
            });*/

            return response()->json([
                'message' => 'enquiry is inserted'
            ],200);
        }
    }

    public function store(Request $request)
    {
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }
}
