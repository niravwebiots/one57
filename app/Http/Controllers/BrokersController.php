<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Broker;

class BrokersController extends Controller
{
    public function index()
    {
        $brokers = Broker::get();
        return view('admin.brokers.index',compact('brokers'));
    }

    public function create()
    {
        return view('admin.brokers.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'question_name' => 'required|max:255|regex:/(^[A-Za-z, ]+$)+/',
        ]);

        $broker = new Broker;
        $broker->broker_name=$request->question_name;

        if($broker->save()){
            
            $notification = array(
                'message' => 'Broker is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('brokers.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $broker = Broker::find($id);
        return view('admin.brokers.edit',compact('broker'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'question_name' => 'required|max:255|regex:/(^[A-Za-z, ]+$)+/',
        ]);

        $broker = Broker::find($id);
        $broker->broker_name=$request->question_name;

        if($broker->save()){
            
            $notification = array(
                'message' => 'Broker is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('brokers.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $broker = Broker::findOrFail($id);
        $broker->delete();

        $notification = array(
            'message' => 'Broker is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('brokers.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $broker = Broker::find($id);
        $broker->status = $opr;
        $broker->save();

        $notification = array(
            'message' => 'Broker status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('brokers.index')->with($notification);
    }

    public function get_contact_brokers()
    {
        $brokers = Broker::where('status',1)->get();
        return json_encode($brokers);
    }
}
