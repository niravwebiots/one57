<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::get();
        return view('admin.countries.index',compact('countries'));
    }

    public function create()
    {
        return view('admin.countries.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'country_name' => 'required|max:255|regex:/(^[A-Za-z ]+$)+/',
        ]);

        $country = new Country;
        $country->country_name=$request->country_name;

        if($country->save()){
            
            $notification = array(
                'message' => 'Country is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('countries.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $country = Country::find($id);
        return view('admin.countries.edit',compact('country'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'country_name' => 'required|max:255|regex:/(^[A-Za-z ]+$)+/',
        ]);

        $country = Country::find($id);
        $country->country_name=$request->country_name;

        if($country->save()){
            
            $notification = array(
                'message' => 'Country is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('countries.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();

        $notification = array(
            'message' => 'Country is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('countries.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $country = Country::find($id);
        $country->status = $opr;
        $country->save();

        $notification = array(
            'message' => 'Country status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('countries.index')->with($notification);
    }

    public function get_contact_country()
    {
        $countries = Country::where('status',1)->get();
        return json_encode($countries);
    }
}
