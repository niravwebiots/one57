<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use File;

class GalleriesController extends Controller
{
    public function index()
    {
        $galleries = Gallery::get();
        return view('admin.galleries.index',compact('galleries'));
    }

    public function create()
    {
        return view('admin.galleries.create_gallery');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'gallery_category' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $gallery = new Gallery;
        $gallery->title=$request->title;
        $gallery->category=$request->gallery_category;

        if($request->gallery_type == 'image' && $request->file('image') )
        {
            $gallery_image = $request->file('image');

            $gallery->image = date('YmdHis').".".$gallery_image->getClientOriginalExtension();

            $destination = public_path('uploads/galleries/');

            $gallery_image->move($destination,$gallery->image);
        }
        else if($request->gallery_type == 'video' && $request->video_url)
        {
            $gallery->video_url = $request->video_url;
        }
        else
        {
            $this->validate($request,[
                'title' => 'required|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
                'gallery_category' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
        }
        
        if($gallery->save()){
            
            $notification = array(
                'message' => 'Gallery is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('galleries.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $gallery = Gallery::find($id);
        return view('admin.galleries.show_gallery',compact('gallery'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'gallery_category' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $gallery = Gallery::find($id);
        $gallery->title=$request->title;

        if($request->gallery_type == 'image' && $request->hasFile('image'))
        {
            $image_path = "uploads/galleries/".$gallery->image;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            
            $gallery_image = $request->file('image');
            $gallery->image = date('YmdHis').".".$gallery_image->getClientOriginalExtension();
            $destination = public_path('uploads/galleries/');
            
            $gallery_image->move($destination,$gallery->image);

            $gallery->video_url = $request->video_url;            
        }
        else if($request->gallery_type == 'video' && $request->video_url)
        {
            $image_path = "uploads/galleries/".$gallery->image;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $gallery->image = "";

            $gallery->video_url = $request->video_url;
        }
        
        if($gallery->save()){
            
            $notification = array(
                'message' => 'Gallery is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('galleries.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);

        /*$gallery->image;

        $image_path = "uploads/galleries/".$gallery->image;
    
        if(File::exists($image_path)) {
            File::delete($image_path);
        }       */

        $gallery->delete();

        $notification = array(
            'message' => 'Gallery is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('galleries.index')->with($notification);
    }
    
    public function status($id,$opr)
    {
        $gallery = Gallery::find($id);
        $gallery->status = $opr;
        $gallery->save();

        $notification = array(
            'message' => 'Gallery status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('galleries.index')->with($notification);
    }

    public function get_galleries($type)
    {
         $gallery = Gallery::where('category',$type)->where('status',1)->get();

         $list = collect();

        foreach ($gallery as $value) {

          $list->push(collect([
                'thumb' => '/uploads/galleries/'.$value->image,
                'src' => '/uploads/galleries/'.$value->image,
                'caption' => $value->title
          ]));
        }
        return json_encode($list);
    }

}
