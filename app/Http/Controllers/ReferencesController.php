<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reference;

class ReferencesController extends Controller
{
    public function index()
    {
        $references = Reference::get();
        return view('admin.references.index',compact('references'));
    }

    public function create()
    {
        return view('admin.references.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'reference_name' => 'required|max:255|regex:/(^[A-Za-z, ]+$)+/',
        ]);

        $reference = new Reference;
        $reference->reference_name=$request->reference_name;

        if($reference->save()){
            
            $notification = array(
                'message' => 'Reference is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('references.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $reference = Reference::find($id);
        return view('admin.references.edit',compact('reference'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'reference_name' => 'required|max:255|regex:/(^[A-Za-z, ]+$)+/',
        ]);

        $reference = Reference::find($id);
        $reference->reference_name=$request->reference_name;

        if($reference->save()){
            
            $notification = array(
                'message' => 'Reference is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('references.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $reference = Reference::findOrFail($id);
        $reference->delete();

        $notification = array(
            'message' => 'Reference is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('references.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $reference = Reference::find($id);
        $reference->status = $opr;
        $reference->save();

        $notification = array(
            'message' => 'Reference status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('references.index')->with($notification);
    }

    public function get_contact_references()
    {
        $references = Reference::where('status',1)->get();
        return json_encode($references);
    }
}
