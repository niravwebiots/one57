<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Homesize;

class HomesizesController extends Controller
{
    public function index()
    {
        $homesizes = Homesize::get();
        return view('admin.homesizes.index',compact('homesizes'));
    }

    public function create()
    {
        return view('admin.homesizes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'homesize_name' => 'required|max:255|regex:/(^[A-Za-z0-9- ]+$)+/',
        ]);

        $homesize = new Homesize;
        $homesize->size_name=$request->homesize_name;

        if($homesize->save()){
            
            $notification = array(
                'message' => 'Homesize is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('homesizes.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $homesize = Homesize::find($id);
        return view('admin.homesizes.edit',compact('homesize'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'homesize_name' => 'required|max:255|regex:/(^[A-Za-z0-9- ]+$)+/',
        ]);

        $homesize = Homesize::find($id);
        $homesize->size_name=$request->homesize_name;

        if($homesize->save()){
            
            $notification = array(
                'message' => 'Homesize is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('homesizes.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $homesize = Homesize::findOrFail($id);
        $homesize->delete();

        $notification = array(
            'message' => 'Homesize is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('homesizes.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $homesize = Homesize::find($id);
        $homesize->status = $opr;
        $homesize->save();

        $notification = array(
            'message' => 'Homesize status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('homesizes.index')->with($notification);
    }

    public function get_contact_homesizes()
    {
        $homesizes = Homesize::where('status',1)->get();
        return json_encode($homesizes);
    }
}
