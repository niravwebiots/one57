<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function get_option($type)
    {
        $option = Option::where('type',$type)->first();
        return json_encode($option);
    }

    public function set_contact_us(Request $request)
    {
        $updates = array('description' => $request->contact_description);

        if(Option::where('type',$request->type)->update($updates))
        {
            $notification = array(
                'message' => 'Contact Description is Update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('contactus.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }
}
