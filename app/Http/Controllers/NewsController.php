<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use File;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::get();
        return view('admin.news.index',compact('news'));
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'magazine_name' => 'required|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'magazine_image' => 'required|mimes:jpeg,jpg,png',
            'news_date' => 'required',
            'news_image' => 'required|mimes:jpeg,jpg,png',
            'news_title' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
            'medium' => 'required',
            'news_link' => 'required|url',
            'news_description' => 'required'
        ]);

        $news = new News();
        
        $news->magazine_name = $request->magazine_name;
        $news->date = $request->news_date;
        $news->title = $request->news_title;
        $news->medium = $request->medium;
        $news->slug = kebab_case($request->news_title);
        $news->description = $request->news_description;
        $news->link = $request->news_link;

        $magazine_image = $request->file('magazine_image');
        $news->magazine_logo = date('YmdHis')."logo".".".$magazine_image->getClientOriginalExtension();
        $destination = public_path('uploads/news/');
        $magazine_image->move($destination,$news->magazine_logo);

        $news_image = $request->file('news_image');
        $news->image = date('YmdHis')."news".".".$news_image->getClientOriginalExtension();
        $destination = public_path('uploads/news/');
        $news_image->move($destination,$news->image);

        if($news->save()){
            
            $notification = array(
                'message' => 'News is Insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('news.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.edit',compact('news'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'magazine_name' => 'required|max:255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'magazine_image' => 'mimes:jpeg,jpg,png',
            'news_date' => 'required',
            'news_image' => 'mimes:jpeg,jpg,png',
            'news_title' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
            'medium' => 'required',
            'news_link' => 'required|url',
            'news_description' => 'required'
        ]);

        $news = News::find($id);
        
        $news->magazine_name = $request->magazine_name;
        $news->date = $request->news_date;
        $news->title = $request->news_title;
        $news->medium = $request->medium;
        $news->slug = kebab_case($request->news_title);
        $news->description = $request->news_description;
        $news->link = $request->news_link;

        if($request->hasFile('magazine_image'))
        {
            $image_path = "uploads/news/".$news->magazine_logo;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $magazine_image = $request->file('magazine_image');
            $news->magazine_logo = date('YmdHis')."logo".".".$magazine_image->getClientOriginalExtension();
            $destination = public_path('uploads/news/');
            $magazine_image->move($destination,$news->magazine_logo);
        }

        if($request->hasFile('news_image'))
        {
            $image_path = "uploads/news/".$news->image;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $news_image = $request->file('news_image');
            $news->image = date('YmdHis')."news".".".$news_image->getClientOriginalExtension();
            $destination = public_path('uploads/news/');
            $news_image->move($destination,$news->image);
        }

        if($news->save()){
            
            $notification = array(
                'message' => 'News is Update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('news.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $news = News::findOrFail($id);

        /*$magazine_logo = "uploads/news/".$news->magazine_logo;
    
        if(File::exists($magazine_logo)) {
            File::delete($magazine_logo);
        }

        $image = "uploads/news/".$news->image;
    
        if(File::exists($image)) {
            File::delete($image);
        }*/

        $news->delete();

        $notification = array(
            'message' => 'News is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('news.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $news = News::find($id);
        $news->status = $opr;
        $news->save();

        $notification = array(
            'message' => 'News status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('news.index')->with($notification);
    }

    public function get_news()
    {
       $news = News::where('status',1)->orderBy('created_at','desc')->get();
       return json_encode($news);
    }

    public function get_single_news($slug)
    {
        $news['single'] = News::where('slug',$slug)->where('status',1)->get()->first();
        $news['prev'] = News::where('id', '<', $news['single']['id'])->where('status',1)->get()->first();
        $news['next'] = News::where('id', '>', $news['single']['id'])->where('status',1)->get()->first();
        $news['last'] = News::all()->last();
        return json_encode($news);
    }
}
