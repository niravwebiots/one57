<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Welcome;
use File;

class WelcomeController extends Controller
{
    public function index()
    {
        $welcome = Welcome::get();
        return view('admin.welcome.index',compact('welcome'));
    }

    public function create()
    {
        return view('admin.welcome.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'headline' => 'required|max:255',
            'button_title' =>'required',
            'link' => 'required',
            'background_url' => 'required|url',
            'introcontent' => 'required',
        ]);

        $welcome = new Welcome();
        $welcome->headline = $request->headline;
        $welcome->button_title = $request->button_title;
        $welcome->link = $request->link;
        $welcome->introcontent = $request->introcontent;
        $welcome->background_url = $request->background_url;


        /*$background_url = $request->file('background_url');
        $welcome->background_url = date('YmdHis').".".$background_url->getClientOriginalExtension();
        $destination = public_path('uploads/welcome/');
        $background_url->move($destination,$welcome->background_url);*/

        if($welcome->save()){
            
            $notification = array(
                'message' => 'Welcome is Insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('welcome.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $welcome = Welcome::find($id);
        return view('admin.welcome.edit',compact('welcome'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'headline' => 'required|max:255',
            'button_title' =>'required',
            'link' => 'required',
            'background_url' => 'url',
            'introcontent' => 'required',
        ]);

        $welcome = Welcome::find($id);
        $welcome->headline = $request->headline;
        $welcome->button_title = $request->button_title;
        $welcome->link = $request->link;
        $welcome->introcontent = $request->introcontent;
        $welcome->background_url = $request->background_url;

        /*if($request->hasFile('background_url'))
        {
            $image_path = "uploads/welcome/".$welcome->background_url;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $background_url = $request->file('background_url');
            //$welcome->background_url = date('YmdHis').".".$background_url->getClientOriginalExtension();
            $welcome->background_url = 'videos.'.$background_url->getClientOriginalExtension();
            $destination = public_path('uploads/welcome/');
            $background_url->move($destination,$welcome->background_url);
        }*/

        if($welcome->save()){
            
            $notification = array(
                'message' => 'Welcome is Update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('welcome.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $welcome = Welcome::findOrFail($id);

        /*$background_url = "uploads/welcome/".$welcome->background_url;
    
        if(File::exists($background_url)) {
            File::delete($background_url);
        }*/

        $welcome->delete();

        $notification = array(
            'message' => 'Welcome is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('welcome.index')->with($notification);
    }

    public function status($id,$opr)
    {
        if($opr == 1)
        {
            $wel = Welcome::where('status',1)->count();

            if($wel >= 1)
            {
                $notification = array(
                    'message' => 'Sorry You Can Active only one Welcome record at a Time.', 
                    'alert-type' => 'warning'
                );

                return redirect()->route('welcome.index')->with($notification);
            }
            else
            {
                $welcome = Welcome::find($id);
                $welcome->status = $opr;
                $welcome->save();

                $notification = array(
                    'message' => 'Welcome status update Successfully.', 
                    'alert-type' => 'success'
                );
                
                return redirect()->route('welcome.index')->with($notification);
            }
        }
        else
        {
            $welcome = Welcome::find($id);
            $welcome->status = $opr;
            $welcome->save();

            $notification = array(
                'message' => 'Welcome status update Successfully.', 
                'alert-type' => 'success'
            );
            
            return redirect()->route('welcome.index')->with($notification);
        }
    }


    public function get_home_welcome()
    {
        $welcome = Welcome::where('status',1)->first();
        return json_encode($welcome);
    }
}
