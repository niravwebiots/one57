<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomeSlider;
use File;

class HomeSliderController extends Controller
{
    public function index()
    {
        $sliders = HomeSlider::orderBy('orderno')->get();
        return view('admin.homeslider.index',compact('sliders'));
    }
    public function create()
    {
        return view('admin.homeslider.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'subtitle' => 'required|max:255',
            'link' => 'required|url',
            'background_image' => 'required|mimes:jpeg,jpg,png',
            'font_color' => 'required',
            'background_color' => 'required',
            'description' => 'required',
        ]);

        $slider = new HomeSlider();
        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->description = $request->description;
        $slider->link = $request->link;
        $slider->font_color = $request->font_color;
        $slider->background_color = $request->background_color;

        $slider_image = $request->file('background_image');
        $slider->background_image = date('YmdHis').".".$slider_image->getClientOriginalExtension();
        $destination = public_path('uploads/homeslider/');
        $slider_image->move($destination,$slider->background_image);

        if($slider->save()){
            
            $notification = array(
                'message' => 'Slider is Insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('homeslider.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }
    
    public function show($id)
    {
    }

    public function edit($id)
    {
        $slider = HomeSlider::find($id);

        return view('admin.homeslider.edit',compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'subtitle' => 'required|max:255',
            'link' => 'required|url',
            'background_image' => 'mimes:jpeg,jpg,png',
            'font_color' => 'required',
            'background_color' => 'required',
            'description' => 'required',
        ]);

        $slider = HomeSlider::find($id);
        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->description = $request->description;
        $slider->link = $request->link;
        $slider->font_color = $request->font_color;
        $slider->background_color = $request->background_color;

        if($request->hasFile('background_image'))
        {
            $image_path = "uploads/homeslider/".$slider->image;
        
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $slider_image = $request->file('background_image');
            $slider->background_image = date('YmdHis').".".$slider_image->getClientOriginalExtension();
            $destination = public_path('uploads/homeslider/');
            $slider_image->move($destination,$slider->background_image);
        }

        if($slider->save()){
            
            $notification = array(
                'message' => 'Slider is Update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('homeslider.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $slider = HomeSlider::findOrFail($id);

        /*$background_image = "uploads/homeslider/".$slider->background_image;
    
        if(File::exists($background_image)) {
            File::delete($background_image);
        }*/

        $slider->delete();

        $notification = array(
            'message' => 'Slider is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('homeslider.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $slider = HomeSlider::find($id);
        $slider->status = $opr;
        $slider->save();

        $notification = array(
            'message' => 'Home Slider status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('homeslider.index')->with($notification);
    }

    public function set_sliders_position(Request $request)
    {
        foreach ($request->newPosition as $key => $value)
        {
            $slider = HomeSlider::where('id',$key)->update(['orderno'=>$value]);
        }

        /*$response['status'] = 'success';
        echo json_encode($response);*/
    }

    public function get_homesliders()
    {
        $sliders = HomeSlider::orderBy('orderno')->get();
        return json_encode($sliders);
    }
}
