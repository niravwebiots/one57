<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutUs;

class AboutUsTabsController extends Controller
{
    public function index()
    {
        $aboutus = AboutUs::where('type',1)->get();
        return view('admin.aboutustabs.index',compact('aboutus'));
    }

    public function create()
    {
        return view('admin.aboutustabs.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'main_title' => 'required|max:255',
            'sub_title' => 'max:255',
            'description' => 'required',
        ]);

        $section = new AboutUs;
        $section->main_title=$request->main_title;
        $section->sub_title=$request->sub_title;
        $section->description=$request->description;
        $section->type = 0;

        if($section->save()){
            
            $notification = array(
                'message' => 'Section is insert Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('aboutustabs.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $aboutus = AboutUs::find($id);
        return view('admin.aboutustabs.edit',compact('aboutus'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'main_title' => 'required|max:255',
            'sub_title' => 'max:255',
            'description' => 'required',
        ]);

        $section = AboutUs::find($id);
        $section->main_title=$request->main_title;
        $section->sub_title=$request->sub_title;
        $section->description=$request->description;
        $section->type = 0;

        if($section->save()){
            
            $notification = array(
                'message' => 'Section is update Successfully.', 
                'alert-type' => 'success'
            );

            return redirect()->route('aboutustabs.index')->with($notification);
        }else{
            return redirect()->back()->route();
        }
    }

    public function destroy($id)
    {
        $section = AboutUs::findOrFail($id);
        $section->delete();

        $notification = array(
            'message' => 'Section is delete Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('aboutustabs.index')->with($notification);
    }

    public function status($id,$opr)
    {
        $section = AboutUs::find($id);
        $section->status = $opr;
        $section->save();

        $notification = array(
            'message' => 'Section status update Successfully.', 
            'alert-type' => 'success'
        );

        return redirect()->route('aboutustabs.index')->with($notification);
    }
}
