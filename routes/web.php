<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 

Auth::routes();

Route::group(['middleware' => ['admin','auth']], function () {
    Route::prefix('admin')->group(function () {
    // Route::namespace('admin')->group(function () {


        Route::get('dashboard', 'HomeController@index')->name('admin.dashboard');

        Route::resource('galleries','GalleriesController');
        Route::get('galleries-status/{id}/{opr}','GalleriesController@status')->name('admin.galleries.status');

        Route::resource('news','NewsController');
        Route::get('news-status/{id}/{opr}','NewsController@status')->name('admin.news.status');

        Route::resource('welcome','WelcomeController');
        Route::get('welcome-status/{id}/{opr}','WelcomeController@status')->name('admin.welcome.status');

        Route::resource('homeslider','HomeSliderController');
        Route::get('homeslider-status/{id}/{opr}','HomeSliderController@status')->name('admin.homeslider.status');
        Route::post('homeslider-set-position','HomeSliderController@set_sliders_position')->name('admin.homeslider.set.position');

        Route::resource('contactus','ContactusController');
        Route::post('set-contact','OptionsController@set_contact_us')->name('admin.set.contact');

        Route::resource('countries','CountriesController');
        Route::get('countries-status/{id}/{opr}','CountriesController@status')->name('admin.countries.status');

        Route::resource('homesizes','HomesizesController');
        Route::get('homesizes-status/{id}/{opr}','HomesizesController@status')->name('admin.homesizes.status');

        Route::resource('brokers','BrokersController');
        Route::get('brokers-status/{id}/{opr}','BrokersController@status')->name('admin.brokers.status');

        Route::resource('references','ReferencesController');
        Route::get('references-status/{id}/{opr}','ReferencesController@status')->name('admin.references.status');

        Route::resource('aboutus','AboutUsController');
        Route::get('aboutus-status/{id}/{opr}','AboutUsController@status')->name('admin.aboutus.status');

        Route::resource('aboutustabs','AboutUsTabsController');
        Route::get('aboutustabs-status/{id}/{opr}','AboutUsTabsController@status')->name('admin.aboutustabs.status');

    // });
  });
});


Route::get('/',function(){
    return redirect('main');
});

/* get data from below route */

Route::get('get-home-welcome','WelcomeController@get_home_welcome');
Route::get('get-contact-country','CountriesController@get_contact_country');
Route::get('get-contact-homesizes','HomesizesController@get_contact_homesizes');
Route::get('get-contact-brokers','BrokersController@get_contact_brokers');
Route::get('get-contact-references','ReferencesController@get_contact_references');
Route::get('get-option/{type}','OptionsController@get_option');
Route::post('apply-for-enquiry','ContactusController@create');

Route::get('get-home-sliders','HomeSliderController@get_homesliders');
Route::get('get-news','NewsController@get_news');
Route::get('get-single-news/{slug}','NewsController@get_single_news');

Route::get('get-galleries/{type}','GalleriesController@get_galleries');

/* dont touch the above path */

/* Below Is Vue Router Path */
Route::get('vue/{any?}/{id?}', function () {
    return view('front.home');
});


/* Below Is PHP Page Path */
Route::get('main', function () {
    return view('main');
})->name('main');

Route::get('slider', function () {
    return view('slider');
})->name('slider');

Route::get('gallery', function () {
    return view('gallery');
})->name('gallery');

Route::get('contact', function () {
    return view('contact');
})->name('contact');

Route::get('slide', function () {
    return view('slide');
})->name('slide'); 

Route::get('news', function () {
    return view('news');
})->name('news');

Route::get('smart&security', function () {
    return view('smart&security');
})->name('smart&security');

Route::get('amenities', function () {
    return view('amenities');
})->name('amenities');

Route::get('ace', function () {
    return view('ace');
})->name('ace');

Route::get('lifestyle', function () {
    return view('lifestyle');
})->name('lifestyle');

Route::get('location', function () {
    return view('location');
})->name('location');

Route::get('ssgf', function () {
    return view('ssgf');
})->name('ssgf');

Route::get('floor_plan', function () {
    return view('floor_plan');
})->name('floor_plan');

Route::get('views', function () {
    return view('views');
})->name('views');

Route::get('about', function () {
    return view('about');
})->name('about');

Route::get('single_news', function () {
    return view('single_news');
})->name('single_news');


Route::get('/dashboard',function(){
    return redirect('dashboard.index');
});


Route::prefix('dashboard')->group(function () {
    
    Route::get('index', function () {
        return view('dashboard/index');
    })->name('dashboard.index');

    Route::get('gallery', function () {
        return view('dashboard/gallery');
    })->name('dashboard.gallery');

    Route::get('news', function () {
        return view('dashboard/news');
    })->name('dashboard.news');

});
