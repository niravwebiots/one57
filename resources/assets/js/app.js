
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLazyLoad from 'vue-lazyload'

Vue.use(VueRouter)
Vue.use(require('vue-moment'));
Vue.use(VueLazyLoad)
require('vue-image-lightbox/dist/vue-image-lightbox.min.css')


import App from './components/ExampleComponent';

import Welcome from './pages/welcome';
import Contactus from './pages/contactus';
import Homesliders from './pages/homesliders';
import News from './pages/news';
import SingleNews from './pages/single_news';
import Gallery from './pages/gallery';
import Menubar from './components/menubar';

import Sliders from './pages/sliders';
import Vueswiper from './pages/vueswiper';

Vue.component('App', require('./components/ExampleComponent.vue'));
Vue.component('Menubar', Menubar);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
    		  path: '/vue',
    		  name: 'welcome',
    		  component: Welcome
      	},
        {
          path: '/vue/homesliders',
          name: 'homesliders',
          component: Homesliders
        },
        {
          path: '/vue/news',
          name: 'news',
          component: News
        },
        {
          path: '/vue/single-news/:slug',
          name: 'single-news',
          component: SingleNews,
          props:true
        },
        {
          path: '/vue/gallery',
          name: 'gallery',
          component: Gallery
        },
        {
          path: '/vue/contact',
          name: 'contactus',
          component: Contactus
        },
    ],
})

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
