<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>


<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>

<div id="wall">

    <!-- home section -->
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
            <div class="container-fluid header-content ace_firstslider_content">
                <div class="d-flex text-center align-items-center" style="bottom: 0">
                    <div class="text-overlay">
                        <h1>SIGNATURE COLLECTION</h1>
                        <p>One57 sits at the epicenter of Manhattan where The Plaza District meets Central Park. Often referred to as 'Billionaire’s Row', it is the ultimate landscape of society, culture and commerce. Much of New York’s iconography and mythology was born in this neighborhood.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
        </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>LifeStyle</li>
        <li>Living room</li>
        <li>Project kitchen</li>
        <li>Project Bedroom</li>
        <li>Project Bathroom</li>
    </ul>
</div>

<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>

<script>

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>

</body>
</html>
