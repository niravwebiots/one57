<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>

    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<style>
    .single_news{
        width: 100%;
        align-items: center;
        display: flex;
    }
    .single_news ul li{
        display: inline;
        margin: 0 15px;
    }
    .single_news ul li a{
        color: #b7b7b7;
        font-size: 20px;
    }
    .single_news ul li a:hover{
        text-decoration: none;
    }
    .single_news ul .last_next{
        float: right;
    }
    .single_news_div{
        width: 90%;
        margin: 150px auto 0px!important;
    }
    .single_news_img img{
        width: 350px;
    }
    .single_news_content_img{
        display: flex;
    }
    .single_news_content_img .single_news_content{
        width: 60%;
        margin-left: 50px;
    }
    .single_news_content_img .single_news_content p{
        text-align: justify;
        font-family: 'Montserrat', sans-serif;
        letter-spacing: 1.5px;
        font-size: 14px;
        color: #5d5d5d;
        font-weight: 300;
    }
    .single_news_content h5{
        color: #b7b7b7;
        font-size: 20px;
    }
    .news_border{
        border: 1px solid #b7b7b7;
        margin: 20px 0px;
    }
    .single_news ul{
        margin: 0px!important;
        padding: 0px;
        width: 100%;
    }
    .first_news_li{
        margin: 0px!important;
    }
    .single_news_content .single_news_btn{
        margin-top: 10px;
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        height: 36px;
        padding: 2px 14px 0px 14px;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
        background-color: #ebebeb;
        color: #333;
        font-size: 16px;
        letter-spacing: 1px;
        line-height: 1;
        text-transform: uppercase;
        -webkit-transition: background-color 150ms;
        -o-transition: background-color 150ms;
        transition: background-color 150ms;
    }

</style>
<body>

<nav  class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top about_as_black" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>
<section>
    <div class="container">
        <div class="single_news_div">
            <div class="single_news">
                <ul>
                    <li class="first_news_li"><a href="{{ route('news') }}">Back</a></li>
                        <li class="last_next"><a href="#" >Last</a></li>
                        <li class="last_next"><a href="#" >Next</a></li>
                </ul>
            </div>
            <div class="news_border"></div>
            <div class="single_news_content_img">
                <div class="single_news_img">
                    <img src="{{ asset('front/images/slider_1.jpg') }}" class="img-fluid" alt="First image">
                </div>
                <div class="single_news_content">
                    <h5>OPEN HOUSE TV</h5>
                    <h6>January 22, 2019</h6>
                    <h2>Spring Garden Residence with Ryan Serhant</h2>
                    <p>Ryan Serhant of Million Dollar Listing fame recently toured the Spring Garden residence at One57, the collection of Central Park condominiums from Extell. This five-bedroom, six-bath duplex on the 41st floor features interiors by Katherine Newman, complete with a unique glass-rail staircase, a spacious Great Room, and a sunbathed solarium. But, as Serhant’s video tour emphasizes, what makes this home truly special are the breathtaking views wrapping every room through the floor-to-ceiling windows. Residents have unbeatable views of Central Park stretching out below their feet as well as spectacular Midtown and river vistas. Special touches like an extra-large wine cooler make the Spring Garden residence one of the finest to ever debut on Billionaires’ Row.</p>
                    <a class="single_news_btn">View Article</a>
                </div>
            </div>
        </div>
    </div>
</section>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>

</body>
</html>
