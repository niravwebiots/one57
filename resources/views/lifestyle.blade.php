<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>


<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid header-content slider_overlay_content">
    <div class="text-center">
        <div class="text-overlay" id="scollHide">
            <h1>LifeStyle</h1>
            <p>One57 sits at the epicenter of Manhattan where The Plaza District meets Central Park. Often referred to as 'Billionaire’s Row', it is the ultimate landscape of society, culture and commerce. Much of New York’s iconography and mythology was born in this neighborhood.</p>
        </div>
    </div>
</div>
<div id="wall">

    <!-- home section -->
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <div class="img-section">
                <img src="{{ asset('front/images/lifestyle/section-1/header-image.jpg')}}" class="img-fluid" alt="">
            </div>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/lifestyle/section-2/collection-tony-ingrao.webm')}}" type="video/mp4">
            </video>
            <div class="container-fluid header-content ace_firstslider_content">
                <div class="d-flex text-center align-items-center" style="bottom: 0">
                    <div class="text-overlay">
                        <h1>Living Room</h1>
                        <p>Experience The Extraordinary Every Day. Exclusive Amenities Are Paired With The Privileges Of Living Above Park Hyatt New York. A Host Of Private Resident Services And Amenities Cater To The Lifestyle Of Those Who Will Call One57 Home.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/lifestyle/section-3/collection-katherine-newman.webm')}}" type="video/mp4">
            </video>
            <div class="container-fluid header-content ace_firstslider_content">
                <div class="d-flex text-center align-items-center" style="bottom: 0">
                    <div class="text-overlay">
                        <h1>Dining room and Kitchen</h1>
                        <p>Experience The Extraordinary Every Day. Exclusive Amenities Are Paired With The Privileges Of Living Above Park Hyatt New York. A Host Of Private Resident Services And Amenities Cater To The Lifestyle Of Those Who Will Call One57 Home.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/lifestyle/section-4/collection-katherine-newman.webm')}}" type="video/mp4">
            </video>
            <div class="container-fluid header-content ace_firstslider_content">
                <div class="d-flex text-center align-items-center" style="bottom: 0">
                    <div class="text-overlay">
                        <h1>Bed Room</h1>
                        <p>Experience The Extraordinary Every Day. Exclusive Amenities Are Paired With The Privileges Of Living Above Park Hyatt New York. A Host Of Private Resident Services And Amenities Cater To The Lifestyle Of Those Who Will Call One57 Home.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/lifestyle/section-5/collection-bathroom.webm')}}" type="video/mp4">
            </video>
            <div class="container-fluid header-content ace_firstslider_content">
                <div class="d-flex text-center align-items-center" style="bottom: 0">
                    <div class="text-overlay">
                        <h1>Bath Room</h1>
                        <p>Experience The Extraordinary Every Day. Exclusive Amenities Are Paired With The Privileges Of Living Above Park Hyatt New York. A Host Of Private Resident Services And Amenities Cater To The Lifestyle Of Those Who Will Call One57 Home.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>LifeStyle</li>
        <li>Living room</li>
        <li>Dining Room & Kitchen</li>
        <li>Bed Room</li>
        <li>Bath Room</li>
    </ul>
</div>

<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>
<script>
    $(window).bind('mousewheel', function(event) {
        var data_index = $('#wall').attr('data-wall-current-section');
        if (event.originalEvent.wheelDelta >= 0 && data_index == 1 ) {
            $("#scollHide").fadeIn("slow");
        }
        else {
            $("#scollHide").fadeOut("slow");
        }
    });

    var wall = new Wall('#wall',{
        sectionAnimateDuration: 1
    });
</script>
<script>

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>

</body>
</html>
