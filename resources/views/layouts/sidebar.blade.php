<div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        <div>
            <img class="img-50 rounded-circle" src="{{ asset('back/images/user/1.jpg') }}" alt="#">
        </div>
        <h6 class="mt-3 f-12">{{ Auth::user()->name }}</h6>
    </div>
    <ul class="sidebar-menu">
        <li>
            <div class="sidebar-title">General</div>
            <a href="{{ route('admin.dashboard') }}" class="sidebar-header">
                <i class="icon-desktop"></i><span>Dashboard</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-angle-right"></i>Default</a></li>
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-angle-right"></i>E-commerce</a></li>
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-angle-right"></i>Business<span class="badge badge-secondary ml-3">Tour</span></a></li>
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-angle-right"></i>CRM</a></li>
            </ul>
        </li>
        <li>
            <a href="{{ route('galleries.index') }}" class="sidebar-header">
                <i class="icon-desktop"></i><span>Galleries</span>
            </a>
        </li>
        <li>
            <a href="{{ route('news.index') }}" class="sidebar-header">
                <i class="icon-desktop"></i><span>News</span>
            </a>
        </li>
        <li>
            <a href="{{ route('welcome.index') }}" class="sidebar-header">
                <i class="icon-desktop"></i><span>Welcome</span>
            </a>
        </li>
        <li>
            <a href="{{ route('homeslider.index') }}" class="sidebar-header">
                <i class="icon-desktop"></i><span>Home Slider</span>
            </a>
        </li>
        <li>
            <a href="" class="sidebar-header">
                <i class="icon-desktop"></i><span>Contact</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ route('contactus.index') }}"><i class="fa fa-angle-right"></i>Enquires</a></li>
                <li><a href="{{ route('countries.index') }}"><i class="fa fa-angle-right"></i>Countries</a></li>
                <li><a href="{{ route('homesizes.index') }}"><i class="fa fa-angle-right"></i>Homesizes</a></li>
                <li><a href="{{ route('brokers.index') }}"><i class="fa fa-angle-right"></i>Brokers</a></li>
                <li><a href="{{ route('references.index') }}"><i class="fa fa-angle-right"></i>References</a></li>
            </ul>
        </li>

        <li>
            <a href="" class="sidebar-header">
                <i class="icon-desktop"></i><span>About Us</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ route('aboutus.index') }}"><i class="fa fa-angle-right"></i>About Sections</a></li>
                <li><a href="{{ route('aboutustabs.index') }}"><i class="fa fa-angle-right"></i>About Tabs</a></li>
            </ul>
        </li>
    </ul>
</div>