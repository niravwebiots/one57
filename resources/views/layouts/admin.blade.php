<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">

    <link rel="icon" href="{{ asset('back/images/favicon.png') }}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{ asset('back/images/favicon.png') }}" type="image/x-icon" />
    <title>@yield('title')</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/fontawesome.css') }}">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/icofont.css') }}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/themify.css') }}">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/flag-icon.css') }}">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/datatables.css') }}" />

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/bootstrap.css') }}">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/style.css') }}">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/responsive.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/toastr.css') }}">

    @yield('style')
</head>

<body>

<!-- Loader starts -->
<!-- <div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <h4>Have a great day at work today <span>&#x263A;</span></h4>
    </div>
</div> -->
<!-- Loader ends -->

<!--page-wrapper Start-->
<div class="page-wrapper">

    <!--Page Header Start-->
    @include('layouts.header')
    <!--Page Header Ends-->

    <!--Page Body Start-->
    <div class="page-body-wrapper">
        <!--Page Sidebar Start-->
        @include('layouts.sidebar')
        <!--Page Sidebar Ends-->

        <div class="page-body">
            @yield('content')
        </div>
    </div>
    <!--Page Body Ends-->

</div>
<!--page-wrapper Ends-->

<!-- latest jquery-->
<script src="{{ asset('back/js/jquery-3.2.1.min.js') }}"></script>

<!-- Bootstrap js-->
<script src="{{ asset('back/js/popper.min.js') }}"></script>
<script src="{{ asset('back/js/bootstrap.js') }}"></script>

<!-- Sidebar jquery-->
<script src="{{ asset('back/js/sidebar-menu.js') }}"></script>

<!--Datatable js-->
<script src="{{ asset('back/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('back/js/toastr.min.js') }}"></script>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

@yield('script')

</body>
</html>