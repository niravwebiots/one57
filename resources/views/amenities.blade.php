<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>


<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid header-content slider_overlay_content">
    <div class="text-center">
        <div class="text-overlay" id="scollHide">
            <h1>AMENITIES</h1>
            <p>Experience the extraordinary every day. Exclusive amenities are paired with the privileges of living above Park Hyatt New York. A host of private resident services and amenities cater to the lifestyle of those who will call One57 home.</p>
        </div>
    </div>
</div>
<div id="wall">

    <!-- home section -->
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/amenities/section-1/amenities-opener-pool.webm')}}" type="video/mp4">
            </video>
        </div>
    </section>
    <section class="section-1 text-center p-c-c">
 <div class="amenities_header">
        <div class="amenities_first_column">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/amenities/section-2/amenities-pool.webm')}}" type="video/mp4">
            </video>
            <div class="container-fluid  slider_nav_upper_text_set ace_firstslider_content">
                <div class="text-center">
                    <div class="amenities_second_slider">
                        <div class="amenities_second_slider_text">
                            <h4>AMENITIES</h4>
                            <h1>Swimming Pool</h1>
                        </div>
                        <p>Experience the extraordinary every day. Exclusive amenities are paired with the privileges of living above Park Hyatt New York. A host of private resident services and amenities cater to the lifestyle of those who will call One57 home.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="amenities_header">
        <div class="amenities_first_column">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/amenities/section-3/amenities-fitness.webm')}}" type="video/mp4">
            </video>
             <div class="container-fluid  slider_nav_upper_text_set ace_firstslider_content">
                <div class="text-center">
                    <div class="amenities_second_slider">
                        <div class="amenities_second_slider_text">
                            <h4>AMENITIES</h4>
                            <h1>Kids GYM and Kids Play</h1>
                        </div>
                        <p>Experience the extraordinary every day. Exclusive amenities are paired with the privileges of living above Park Hyatt New York. A host of private resident services and amenities cater to the lifestyle of those who will call One57 home.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <section>
        <div class="container-fluid amenities_bg_black">
            <div class="amenities_header">
            <div class="row carousel multiple-items">
                <div class="col-md-6 p-0">
                <div class="amenities_first_column">
                    <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                        <source src="{{ asset('front/images/amenities/section-4/leftside-video.webm')}}" type="video/mp4">
                    </video>
                     <div class="container-fluid  slider_nav_upper_text_set ace_firstslider_content">
                <div class="text-center">
                        <div class="amenities_second_slider_text1">
                            <h4>AMENITIES</h4>
                            <h1>Fitness centre</h1>
                        </div>
                </div>
            </div>
                </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="amenities_second_column">
                        <img src="{{ asset('front/images/amenities/section-4/right_side_image.jpg')}}"  alt="">
                    </div>
                      <div class="container-fluid  slider_nav_upper_text_set ace_firstslider_content">
                <div class="text-center">
                    <div class="amenities_second_slider1">
                        <p>Experience the extraordinary every day. Exclusive amenities are paired with the privileges of living above Park Hyatt New York. A host of private resident services and amenities cater to the lifestyle of those who will call One57 home.</p>
                    </div>
                </div>
            </div>
                </div>
            </div>
            </div><!-- /.carousel -->
    <!-- /.container -->
    </section>
       <section class="section-1 text-center p-c-c">
        <div class="amenities_header">
        <div class="amenities_first_column">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/amenities/section-3/amenities-fitness.webm')}}" type="video/mp4">
            </video>
             <div class="container-fluid  slider_nav_upper_text_set ace_firstslider_content">
                <div class="text-center">
                    <div class="amenities_second_slider">
                        <div class="amenities_second_slider_text">
                            <h4>AMENITIES</h4>
                            <h1>Sky garden</h1>
                        </div>
                        <p>Experience the extraordinary every day. Exclusive amenities are paired with the privileges of living above Park Hyatt New York. A host of private resident services and amenities cater to the lifestyle of those who will call One57 home.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>Amenities</li>
        <li>Swimming fool</li>
        <li>Kids GYM and Kids Play</li>
        <li>Fitness centre</li>
        <li>Sky garden</li>
    </ul>
</div>

<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>

<script>


$(window).bind('mousewheel', function(event) {
    var data_index = $('#wall').attr('data-wall-current-section');
    if (event.originalEvent.wheelDelta >= 0 && data_index == 1 ) {
        $("#scollHide").fadeIn("slow");
    }
    else {
        $("#scollHide").fadeOut("slow");
    }
});

var wall = new Wall('#wall',{
    sectionAnimateDuration: 1
});

  // var config = {
  //             sectionAnimateDuration: 2 
  //           }
            
  //           var wall = new Wall('#wall', config);

</script>

</body>
</html>
