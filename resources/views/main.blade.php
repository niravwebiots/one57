<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">

</head>
<style>
    @keyframes expand{0%{letter-spacing:0px}100%{letter-spacing:7px}}
    .custom_animation{
        animation : expand 5s ease-out forwards;
    }
</style>
<body>

<header>
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <nav class="navbar black"><div class="gradient-bg"></div> <div class="nav-underlay"></div> <div class="logo-nav-holder"><a href="/" class="site-link-component logo-nav active" exact="">One 57</a></div> <div class="hamburger"><div class="icon-bar-wrapper"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div></div> <div id="navlinks"><ul><li class="link-item"><a href="/one57" class="site-link-component router-link-exact-active active">One57</a></li> <li class="link-item"><a href="/location" class="site-link-component">Location</a></li> <li class="link-item mobile-link"><a href="/residences/signature-collection" class="site-link-component mobile-link">Signature Collection</a></li> <li class="link-item mobile-link"><a href="/residences/one-collection" class="site-link-component mobile-link">One Collection</a></li> <li class="desktop-link dropdown-li"><a data-to="res-dropdown" class="">
            Residences
        </a> <div class="nav-dropdown-component res-dropdown"><div class="nav-dropdown-component-arrow"></div> <div class="nav-dropdown-component-content"><div class="nav-dropdown-component-content-wrapper"><div class="flex-contain"><div data-index="0" class="nav-dropdown-option"><div class="description"><h3></h3> <h1>Signature Collection</h1> <p>Three to five bedroom residences on floors 39-90</p></div><a href="" class="site-link-component link"></a></div><div data-index="1" class="nav-dropdown-option"><div class="description"><h3></h3> <h1>One Collection</h1> <p>Fully furnished one to three bedroom residences on floors 32-38</p></div>  <a href="/residences/one-collection" class="site-link-component link"></a></div></div></div></div></div></li> <li class="link-item"><a href="/availability" class="site-link-component">Availability</a></li> <li class="link-item"><a href="/views" class="site-link-component">Views</a></li> <li class="link-item"><a href="/park-hyatt" class="site-link-component">Park Hyatt</a></li> <li class="link-item"><a href="/amenities" class="site-link-component">Amenities</a></li> <li class="link-item"><a href="/news" class="site-link-component">News</a></li> <li class="link-item"><a href="/extell" class="site-link-component">Extell</a></li> <li class="link-item"><a href="/gallery" class="site-link-component">Gallery</a></li> <li class="link-item"><a href="/contact" class="site-link-component">Contact</a></li></ul></div></nav>
        <source src="{{ asset('uploads/welcome/videos.mp4') }}" type="video/mp4">
    </video>
    <div class="container-fluid header-content">
        <div class="d-flex text-center align-items-center">
            <div class="text-overlay">
                <p class="text-overlay-body">Experience an Exclusive Life Style in an Exciting Location, Silicon Valley of Chennai-Perungudi-OMR. Offers Enchanting 5 level landscape garden, Eleven Intelligent smart home systems and Mini Disney with International construction techniques SSGF.</p>
                <h1 class="text-overlay-title custom_animation">The ACE is the Place</h1>
                <p><a href="{{ route('slider') }}"> Explore</a></p>
                <img src="{{ asset('front/images/ace-logo_set.png') }}" alt="logo" class="img-fluid">
            </div>
        </div>
    </div>
    <nav class="navbar navbar-b navbar-trans header_navbar_set navbar-expand-xl fixed-top navbar-responsive-set" id="mainNav">
        <div class="container-fluid">
            <a class="navbar-brand navbar_brand_set" href="#"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
            <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
                @include('menubar')
            </div>
        </div>
    </nav>
</header>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
</body>

</html>
