<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/slick-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>

<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid header-content slider_overlay_content">
    <div class="text-center">
        <div class="text-overlay" id="scollHide">
            <h1>The ACE</h1>
            <p>Experience The Extraordinary Every Day. Exclusive Amenities Are Paired With The Privileges Of Living Above Park Hyatt New York. A Host Of Private Resident Services And Amenities Cater To The Lifestyle Of Those Who Will Call One57 Home.</p>
        </div>
    </div>
</div>
<div id="wall">
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="{{ asset('front/images/Ace/section_1/video.webm')}}" type="video/mp4">
            </video>
        </div>
    </section>
    <section class="ace_middel_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 p-0">
                    <div class="inner-part">
                        <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                            <source src="{{ asset('front/images/Ace/section_2/video-left.webm')}}" type="video/mp4">
                        </video>
                        <div class="container header-content ace_firstslider_content">
                            <div class="d-flex text-center align-items-center" style="bottom: 0">
                                <div class="text-overlay">
                                    <h6>AMENITIES</h6>
                                    <h4>Fitness center</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="inner-part">
                        <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                            <source src="{{ asset('front/images/Ace/section_2/video-left.webm')}}" type="video/mp4">
                        </video>
                        <div class="container header-content ace_firstslider_content">
                            <div class="d-flex text-center align-items-center" style="bottom: 0">
                                <div class="text-overlay">
                                    <h6>AMENITIES</h6>
                                    <h4>Fitness center</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.carousel -->
        </div><!-- /.container -->
    </section>
    <section class="section-1 text-center p-c-c">
        <div class="img-section ace_last_section">
            <img src="{{ asset('front/images/Ace/section_3/Capture.jpg')}}" style="width: auto;">
        </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>The ACE</li>
        <li>Architect</li>
        <li>SSGF</li>
    </ul>
</div>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/slick.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>
<script>
    $(window).bind('mousewheel', function(event) {
        var data_index = $('#wall').attr('data-wall-current-section');
        if (event.originalEvent.wheelDelta >= 0 && data_index == 1 ) {
            $("#scollHide").fadeIn("slow");
        }
        else {
            $("#scollHide").fadeOut("slow");
        }
    });

    var wall = new Wall('#wall',{
        sectionAnimateDuration: 1
    });
</script>
<script>

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>
<script>
    $(document).ready(function(){

        // slick configuration
        $('.multiple-items').slick({
            infinite: true,
            slide: 'figure',
            slidesToScroll: 2,
            slidesToShow: 2,
            speed: 750
        });

        // characters limit
        $("figcaption").each(function(i){
            len=$(this).text().length;
            if(len>55)
            {
                $(this).text($(this).text().substr(0,55)+'...');
            }
        });

    });
</script>
</body>
</html>
