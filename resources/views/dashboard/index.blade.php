<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>The Ace</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('dashboard') }}/css/plugins/morris/morris.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="{{ asset('dashboard') }}/css/datatables.css" rel="stylesheet" type="text/css" />


    <!-- App css -->
    <link href="{{ asset('dashboard') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard') }}/images/ace_logo/logo.ico" rel="shortcut icon" />
    <link href="{{ asset('dashboard') }}/css/style_main.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard') }}/css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">
                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <a href="" class="logo">
                        <img src="{{ asset('dashboard') }}/images/ace_logo/ace-logo_set.png" class="logo">
                    </a>
                </div>
                <!-- End Logo container-->
                <div class="menu-extras topbar-custom">
                    <ul class="list-inline float-right mb-0">
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                <span class="ml-1">Admin <i class="fas fa-angle-down"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a class="dropdown-item" href="#"><i class="far fa-user"></i> Profile</a>

                                <a class="dropdown-item" href="#"><i class="fas fa-cogs"></i> Settings</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>
                        </li>
                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                    </ul>
                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->

        <!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class=" navigation-menu nav nav-tabs">
                        <li class="nav-item has-submenu">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-desktop"></i>Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.gallery') }}"><i class="fas fa-suitcase"></i>Galleries</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.news') }}"><i class="far fa-address-book"></i> NEWS </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-trophy"></i> welcome  </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-file-alt"></i>home slider </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-file-alt"></i>contact  </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->


                    <!-- End navigation menu -->
                </div> <!-- end #navigation -->
            </div> <!-- end container -->
        </div> <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->

    <div class="container-fluid">
        <!-- Page-Title -->
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12 mb-4">
                <div id="morris-bar-example" class="dash-chart"></div>
            </div>
        </div>
    </div>
</div>


<div class="wrapper m-t-30">
    <div class="container">
       <div class="row home">
                              <div class="col-md-12">
                                  <div class="card">
                                      <div class="card-body">
                                          <h4 class="mt-0 header-title">list of enquiries</h4>

                                          <div class="table-responsive">
                                              <table class="table m-t-20 mb-0 table-vertical">

                                                  <tbody>
                                                  <tr>
                                                      <td>
                                                          <img src="images/user-5.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                          Herbert C. Patton
                                                      </td>
                                                      <td>
                                                          hipava@mailinator.net
                                                          <p class="m-0 text-muted font-14">7231471338</p>
                                                      </td>
                                                      <td>
                                                          5/12/2016
                                                          <p class="m-0 text-muted font-14">Date</p>
                                                      </td>
                                                      <td>
                                                          <button type="button" class="btn btn-secondary btn-sm waves-effect">More</button>
                                                      </td>
                                                  </tr>

                                                  <tr>
                                                      <td>
                                                          <img src="images/user-1.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                          Mathias N. Klausen
                                                      </td>
                                                      <td>
                                                          hipava@mailinator.net
                                                          <p class="m-0 text-muted font-14">7231471338</p>
                                                      </td>
                                                      <td>
                                                          10/11/2016
                                                          <p class="m-0 text-muted font-14">Date</p>
                                                      </td>
                                                      <td>
                                                          <button type="button" class="btn btn-secondary btn-sm waves-effect">More</button>
                                                      </td>
                                                  </tr>

                                                  <tr>
                                                      <td>
                                                          <img src="images/user-2.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                          Nikolaj S. Henriksen
                                                      </td>
                                                      <td>
                                                          hipava@mailinator.net
                                                          <p class="m-0 text-muted font-14">7231471338</p>
                                                      </td>
                                                      <td>
                                                          8/11/2016
                                                          <p class="m-0 text-muted font-14">Date</p>
                                                      </td>
                                                      <td>
                                                          <button type="button" class="btn btn-secondary btn-sm waves-effect">More</button>
                                                      </td>
                                                  </tr>

                                                  <tr>
                                                      <td>
                                                          <img src="images/user-3.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                          Lasse C. Overgaard
                                                      </td>
                                                      <td>
                                                          hipava@mailinator.net
                                                          <p class="m-0 text-muted font-14">7231471338</p>
                                                      </td>
                                                      <td>
                                                          7/11/2016
                                                          <p class="m-0 text-muted font-14">Date</p>
                                                      </td>
                                                      <td>
                                                          <button type="button" class="btn btn-secondary btn-sm waves-effect">More</button>
                                                      </td>
                                                  </tr>

                                                  <tr>
                                                      <td>
                                                          <img src="images/user-4.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                          Kasper S. Jessen
                                                      </td>
                                                      <td>
                                                          hipava@mailinator.net
                                                          <p class="m-0 text-muted font-14">7231471338</p>
                                                      </td>
                                                      <td>
                                                          1/11/2016
                                                          <p class="m-0 text-muted font-14">Date</p>
                                                      </td>
                                                      <td>
                                                          <button type="button" class="btn btn-secondary btn-sm waves-effect">More</button>
                                                      </td>
                                                  </tr>

                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                              </div>
       </div>
    </div>
</div>


<!-- end wrapper -->


<!-- Footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->



<!-- jQuery  -->
<script src="{{ asset('dashboard') }}/js/jquery.min.js"></script>
<script src="{{ asset('dashboard') }}/js/popper.min.js"></script>
<script src="{{ asset('dashboard') }}/js/bootstrap.min.js"></script>
<!--Morris Chart-->
<script src="{{ asset('dashboard') }}/plugins/morris/morris.min.js"></script>
<script src="{{ asset('dashboard') }}/plugins/rephel/raphael-min.js"></script>
<script src="{{ asset('dashboard') }}/js/dashborad.js"></script>
<!-- App js -->
<script src="{{ asset('dashboard') }}/js/app.js"></script>





</body>
</html>