<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fonik</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('dashboard') }}/css/plugins/morris/morris.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="{{ asset('dashboard') }}/css/datatables.css" rel="stylesheet" type="text/css" />


    <!-- App css -->
    <link href="{{ asset('dashboard') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard') }}/css/icons/icons.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard') }}/css/style_main.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard') }}/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">

                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <a href="index.html" class="logo">

                    </a>
                    <!-- Image Logo -->
                    <!-- <a href="index.html" class="logo">
                        <img src="assets/images/logo-sm.png" alt="" height="22" class="logo-small">
                        <img src="assets/images/logo.png" alt="" height="24" class="logo-large">
                    </a> -->

                </div>
                <!-- End Logo container-->


                <div class="menu-extras topbar-custom">

                    <ul class="list-inline float-right mb-0">

                        <!-- notification-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <i class="ti-bell noti-icon"></i>
                                <span class="badge badge-info badge-pill noti-icon-badge">3</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>Notification (3)</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                </a>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    View All
                                </a>

                            </div>
                        </li>
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                <span class="ml-1">Denish J. <i class="fas fa-angle-down"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a class="dropdown-item" href="#"><i class="far fa-user"></i> Profile</a>

                                <a class="dropdown-item" href="#"><span class="badge badge-success pull-right m-t-5">5</span><i class="fas fa-cogs"></i> Settings</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>
                        </li>
                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                    </ul>
                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->

        <!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class=" navigation-menu nav nav-tabs">
                        <li class="nav-item has-submenu">
                            <a class="nav-link" href="dashboard.html"><i class="fas fa-desktop"></i>Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="gallery.html"><i class="fas fa-suitcase"></i>Galleries</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="news.html"><i class="far fa-address-book"></i> NEWS </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#menu3"><i class="fas fa-trophy"></i> welcome  </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#menu4"><i class="fas fa-file-alt"></i>home slider </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#menu5"><i class="fas fa-file-alt"></i>contact  </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->


                    <!-- End navigation menu -->
                </div> <!-- end #navigation -->
            </div> <!-- end container -->
        </div> <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->

    <div class="container-fluid">
        <!-- Page-Title -->

        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12 mb-4">
                <div id="morris-bar-example" class="dash-chart"></div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row home">
            <div class="col-xl-8">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-30 header-title">Latest Transactions</h4>
                        <div class="table-responsive">
                            <table class="table m-t-20 mb-0 table-vertical">
                                <tbody>
                                <tr>
                                    <td>
                                        <img src="images/user-5.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                        Herbert C. Patton
                                    </td>
                                    <td><i class="fas fa-circle text-success"></i> Confirm</td>
                                    <td>
                                        $14,584
                                        <p class="m-0 text-muted font-14">Amount</p>
                                    </td>
                                    <td>
                                        5/12/2016
                                        <p class="m-0 text-muted font-14">Date</p>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm waves-effect">Edit</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/user-1.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                        Mathias N. Klausen
                                    </td>
                                    <td><i class="fas fa-circle text-warning"></i> Waiting payment</td>
                                    <td>
                                        $8,541
                                        <p class="m-0 text-muted font-14">Amount</p>
                                    </td>
                                    <td>
                                        10/11/2016
                                        <p class="m-0 text-muted font-14">Date</p>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm waves-effect">Edit</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/user-2.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                        Nikolaj S. Henriksen
                                    </td>
                                    <td><i class="fas fa-circle text-success"></i> Confirm</td>
                                    <td>
                                        $954
                                        <p class="m-0 text-muted font-14">Amount</p>
                                    </td>
                                    <td>
                                        8/11/2016
                                        <p class="m-0 text-muted font-14">Date</p>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm waves-effect">Edit</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="images/user-3.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                        Lasse C. Overgaard
                                    </td>
                                    <td><i class="fas fa-circle text-danger"></i> Payment expired</td>
                                    <td>
                                        $44,584
                                        <p class="m-0 text-muted font-14">Amount</p>
                                    </td>
                                    <td>
                                        7/11/2016
                                        <p class="m-0 text-muted font-14">Date</p>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm waves-effect">Edit</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img src="images/user-4.jpg" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                        Kasper S. Jessen
                                    </td>
                                    <td><i class="fas fa-circle text-suc"></i> Confirm</td>
                                    <td>
                                        $8,844
                                        <p class="m-0 text-muted font-14">Amount</p>
                                    </td>
                                    <td>
                                        1/11/2016
                                        <p class="m-0 text-muted font-14">Date</p>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm waves-effect">Edit</button>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- end wrapper -->


<!-- Footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © 2018 Fonik - Crafted with <i class="fas fa-heart text-danger"></i> by Themesbrand.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->



<!-- jQuery  -->
<script src="{{ asset('dashboard') }}/js/jquery.min.js"></script>
<script src="{{ asset('dashboard') }}/js/popper.min.js"></script>
<script src="{{ asset('dashboard') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('dashboard') }}/js/modernizr.min.js"></script>
{{--<script src="{{ asset('dashboard') }}/js/waves.js"></script>--}}
{{--<script src="{{ asset('dashboard') }}/js/jquery.slimscroll.js"></script>--}}
{{--<script src="{{ asset('dashboard') }}/js/jquery.nicescroll.js"></script>--}}
{{--<script src="{{ asset('dashboard') }}/js/jquery.scrollTo.min.js"></script>--}}

<!--Morris Chart-->
<script src="{{ asset('dashboard') }}/plugins/morris/morris.min.js"></script>
<script src="{{ asset('dashboard') }}/plugins/rephel/raphael-min.js"></script>

<script src="{{ asset('dashboard') }}/js/dashborad.js"></script>

<!-- App js -->
<script src="{{ asset('dashboard') }}/js/app.js"></script>





</body>
</html>