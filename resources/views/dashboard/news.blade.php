<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>The Ace</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('dashboard') }}/css/plugins/morris/morris.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="{{ asset('dashboard') }}/css/datatables.css" rel="stylesheet" type="text/css" />


    <!-- App css -->
    <link href="{{ asset('dashboard') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard') }}/images/ace_logo/logo.ico" rel="shortcut icon" />
    <link href="{{ asset('dashboard') }}/css/style_main.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard') }}/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<div class="header-bg p-0">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">

                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <a href="" class="logo">
                        <img src="images/ace-logo_set.png" alt="logo">

                    </a>
                </div>
                <!-- End Logo container-->


                <div class="menu-extras topbar-custom">

                    <ul class="list-inline float-right mb-0">

                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                <span class="ml-1">Admin<i class="fas fa-angle-down"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a class="dropdown-item" href="#"><i class="far fa-user"></i> Profile</a>

                                <a class="dropdown-item" href="#"><i class="fas fa-cogs"></i> Settings</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>
                        </li>
                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                    </ul>
                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->

        <!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class=" navigation-menu nav nav-tabs">
                        <li class="nav-item has-submenu">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-desktop"></i>Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.gallery') }}"><i class="fas fa-suitcase"></i>Galleries</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.news') }}"><i class="far fa-address-book"></i> NEWS </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-trophy"></i> welcome  </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-file-alt"></i>home slider </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-file-alt"></i>contact  </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->


                    <!-- End navigation menu -->
                </div> <!-- end #navigation -->
            </div> <!-- end container -->
        </div> <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->


</div>



<div class="wrapper m-t-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>News</h5>
                        <div class="card-header-right">
                            <a href="http://realestate.webiots.co.in/admin/galleries/create" class="btn btn-primary">Add New News</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="galleries_wrapper" class="dataTables_wrapper no-footer"><div class="dataTables_length" id="galleries_length">
                                <label>Show <select name="galleries_length" aria-controls="galleries" class="">
                                    <option value="10">10</option><option value="25">25</option><option value="50">50</option>
                                    <option value="100">100</option>
                                </select> entries</label>
                            </div>
                                <div id="galleries_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="" placeholder="" aria-controls="galleries">
                                    </label>
                                </div><table id="galleries" class="display dataTable no-footer" role="grid" aria-describedby="galleries_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="No: activate to sort column ascending" style="width: 99px;">No</th>
                                        <th class="sorting" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="Image: activate to sort column ascending" style="width: 244px;">Image</th>
                                        <th class="sorting" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending" style="width: 121px;">Title</th>
                                        <th class="sorting" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="Order no: activate to sort column ascending" style="width: 193px;">Order no</th>
                                        <th class="sorting" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 154px;">Status</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="galleries" rowspan="1" colspan="1" aria-label="Options: activate to sort column descending" aria-sort="ascending" style="width: 458px;">Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <td class="">1</td>
                                        <td><img src="http://realestate.webiots.co.in/uploads/galleries/20190124082331.jpg" style="width: 100px;"></td>
                                        <td class="">hfgh</td>
                                        <td class=""></td>
                                        <td class="">
                                            <a href="http://realestate.webiots.co.in/admin/galleries-status/1/1" class="text-danger">Deactive</a>
                                        </td>
                                        <td class="sorting_1">
                                            <form action="http://realestate.webiots.co.in/admin/galleries/1" method="POST">
                                                <input type="hidden" name="_token" value="VOxfcef93XDOTDXWh5b3G7qJtj6tIh8QRE5Sytwh">                                            <input type="hidden" name="_method" value="DELETE">
                                                <a href="http://realestate.webiots.co.in/admin/galleries/1/edit" class="btn btn-success">
                                                    <i class="fas fa-pencil-alt"></i> Edit
                                                </a>

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="far fa-trash-alt"></i>
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr></tbody>
                                </table>
                                <div class="dataTables_info" id="galleries_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
                                <div class="dataTables_paginate paging_simple_numbers" id="galleries_paginate">
                                    <a class="paginate_button previous disabled" aria-controls="galleries" data-dt-idx="0" tabindex="0" id="galleries_previous">Previous</a>
                                    <span><a class="paginate_button current" aria-controls="galleries" data-dt-idx="1" tabindex="0">1</a></span>
                                    <a class="paginate_button next disabled" aria-controls="galleries" data-dt-idx="2" tabindex="0" id="galleries_next">Next</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>







</div>


<!-- end wrapper -->


<!-- Footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->



<!-- jQuery  -->
<script src="{{ asset('dashboard') }}/js/jquery.min.js"></script>
<script src="{{ asset('dashboard') }}/js/popper.min.js"></script>
<script src="{{ asset('dashboard') }}/js/bootstrap.min.js"></script>
<!-- App js -->
<script src="{{ asset('dashboard') }}/js/app.js"></script>





</body>
</html>