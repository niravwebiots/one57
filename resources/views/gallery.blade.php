<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gallery</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/fullpage.js/dist/fullpage.min.css">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>
<section class="bg-dark-grey">
    <nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
            <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
            <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
                <ul class="navbar-nav navbar_nav_modify">
                @include('menubar')
                </ul>
            </div>
        </div>
    </nav>
<div class="gallery portfolio-section  zoom-gallery">
            <div class="container">
                <h2>Gallery</h2>
                <div class="row row_center">
                    <div class="col-md-3 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f8.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                </div>
                <div class="gallery_pagination">
                    <div class="pagination1">
                        <a href="#">&laquo;</a>
                        <a data-target="#carouselGallery" data-slide-to="1">1</a>
                        <a data-target="#carouselGallery" data-slide-to="2">2</a>
                        <a data-target="#carouselGallery" data-slide-to="3">3</a>
                        <a data-target="#carouselGallery" data-slide-to="4">4</a>
                        <a data-target="#carouselGallery" data-slide-to="5">5</a>
                        <a data-target="#carouselGallery" data-slide-to="6">6</a>

                        <a href="#">&raquo;</a>
                    </div>
                </div>
                <h2>Overseas</h2>
                <div class="row row_center">
                    <div class="gallery_3col_set">
                    <div class="col-md-3 col-sm-12 p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-12 p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                        </div>
                    <div class="col-md-3 col-sm-12 p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                </div>
                <div class="gallery_pagination">
                    <div class="pagination1">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>
                </div>
                <h2>Constration Status</h2>
                <div class="row row_center">
                    <div class="gallery_3col_set">
                        <div class="col-md-3 col-sm-12 p5">
                            <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                                <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-12 p5">
                            <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                                <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-12 p5">
                            <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                                <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f1.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-sm-2 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="{{ asset('front/images/f8.jpg') }}" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-sm-3 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                    <div class="col-sm-3 gallery-item p5">
                        <a href="#galleryImg1" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                            <img src="http://localhost:8000/front/images/f7.jpg" class="img-fluid img-gallery" alt="First image">
                        </a>
                    </div>
                </div>
                <div class="gallery_pagination">
                    <div class="pagination1">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">&raquo;</a>
                    </div>
                </div>
            </div>
        </div>
</section>


<div class="modal modal-gallary bd-example-modal-xl  fade" id="modalGallery" tabindex="-1" role="dialog" aria-labelledby="modalGalleryLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col">
                            <div id="carouselGallery" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                </div> <!-- /.carousel-inner -->
                            </div> <!-- /.carousel -->
                        </div>
                    </div>
                </div>
            </div> <!-- /.modal-body -->

            <div class="modal-footer">
                <ul class="pagination">
                </ul>
            </div> <!-- /.modal-footer -->
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $('.link-gallery').click(function(){
            var galleryId = $(this).attr('data-target');
            var currentLinkIndex = $(this).index('a[data-target="'+ galleryId +'"]');

            createGallery(galleryId, currentLinkIndex);
            createPagination(galleryId, currentLinkIndex);

            $(galleryId).on('hidden.bs.modal', function (){
                destroyGallry(galleryId);
                destroyPagination(galleryId);
            });

            $(galleryId +' .carousel').on('slid.bs.carousel', function (){
                var currentSlide = $(galleryId +' .carousel .carousel-item.active');
                var currentSlideIndex = currentSlide.index(galleryId +' .carousel .carousel-item');

                setTitle(galleryId, currentSlideIndex);
                setPagination(++currentSlideIndex, true);
            })
        });

        function createGallery(galleryId, currentSlideIndex){
            var galleryBox = $(galleryId + ' .carousel-inner');

            $('a[data-target="'+ galleryId +'"]').each(function(){
                var img = $(this).html();
                var galleryItem = $('<div class="carousel-item">'+ img +'</div>');

                galleryItem.appendTo(galleryBox);
            });

            galleryBox.children('.carousel-item').eq(currentSlideIndex).addClass('active');
            setTitle(galleryId, currentSlideIndex);
        }

        function destroyGallry(galleryId){
            $(galleryId + ' .carousel-inner').html("");
        }

        function createPagination(galleryId, currentSlideIndex){
            var pagination = $(galleryId + ' .pagination');
            var carouselId = $(galleryId).find('.carousel').attr('id');
            var prevLink = $('<li class="modal-prev"><a href="#'+ carouselId +'" data-slide="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>');
            var nextLink = $('<li class="modal-next"><a href="#'+ carouselId +'" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>');

            prevLink.appendTo(pagination);
            nextLink.appendTo(pagination);

            $('a[data-target="'+ galleryId +'"]').each(function(){
                var linkIndex = $(this).index('a[data-target="'+ galleryId +'"]');
                var paginationLink = $('<li><a data-target="#carouselGallery" data-slide-to="'+ linkIndex +'">'+ (linkIndex+1) +'</a></li>');

                paginationLink.insertBefore('.pagination li:last-child');
            });

            setPagination(++currentSlideIndex);
        }

        function setPagination(currentSlideIndex, reset = false){
            if (reset){
                $('.pagination li').removeClass('active');
            }

            $('.pagination li').eq(currentSlideIndex).addClass('active');
        }

        function destroyPagination(galleryId){
            $(galleryId + ' .pagination').html("");
        }

        function setTitle(galleryId, currentSlideIndex){
            var imgAlt = $(galleryId + ' .carousel-item').eq(currentSlideIndex).find('img').attr('alt');

            $('.modal-title').text(imgAlt);
        }
    });
</script>
</body>
</html>
