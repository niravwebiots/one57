
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('ace') }}">The ACE</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('location') }}">Location</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('views') }}">Views</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('amenities') }}">Amenities</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('lifestyle') }}">Life Style</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('smart&security') }}">Smart & Security</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('ssgf') }}">SSGF</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('floor_plan') }}">Floor Plan</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('gallery') }}">Gallery</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('about') }}">About Us</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('news') }}">News</a>
    </li>
    <li class="nav-item">
        <a class="nav-link js-scroll" href="{{ route('contact') }}">Contact Us</a>
    </li>
