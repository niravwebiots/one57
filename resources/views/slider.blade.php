<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link href="//cdn.shopify.com/s/files/1/0032/3279/2674/t/60/assets/slick.css?8588643287693252372" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0032/3279/2674/t/60/assets/slick-theme.css?8588643287693252372" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">

</head>
<style>
    .bg_img{
        width: 100%;
        height: 86vh;
        background-repeat: no-repeat;
    }

</style>
<body>
<section class="home_slider_section bg-black">
    <nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
            <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
            <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
                <ul class="navbar-nav navbar_nav_modify">
                @include('menubar')
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="home-wrapper">
            <div class=" home_slider" style="background: black;">
                <div class="home_slider_set">
                    <div class="slider_1">
                    </div>
                </div>
                <div class="home_slider_set">
                    <div class="slider_1">
                    </div>
                </div>
                <div class="home_slider_set">
                    <div class="slider_1">
                    </div>
                </div>
                <div class="home_slider_set">
                    <div class="slider_1">
                    </div>
                </div>
            </div>
            <div class="scroll-section-wrapper">
                <div class="scroll-section-frame subtitle-frame">
                    <div class="scroll-section">
                        <h5>Building and Architecture</h5>
                    </div>
                    <div class="scroll-section">
                        <h5>Where the Plaza District Meets Central Park</h5>
                    </div>
                    <div class="scroll-section">
                        <h5>Residences</h5>
                    </div>
                    <div class="scroll-section">
                        <h5>hotel services</h5>
                    </div>
                </div>
                <div class="scroll-section-frame title-frame">
                    <div class="scroll-section" >
                        <h1>One 57</h1>
                    </div>
                    <div class="scroll-section" >
                        <h1>Location</h1>
                    </div>
                    <div class="scroll-section" >
                        <h1>Signature Collection</h1>
                    </div>
                    <div class="scroll-section" >
                        <h1>One Collection</h1>
                    </div>
                </div>
                <div class="scroll-section-frame copy-frame">
                    <div class="scroll-section" >
                        <p>&nbsp;</p>
                    </div>
                    <div class="scroll-section">
                        <p>&nbsp;</p>
                    </div>
                    <div class="scroll-section">
                        <p>Three to five bedroom residences on floors 39-90</p>
                    </div>
                    <div class="scroll-section">
                        <p>Fully furnished one to three bedroom residences</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src="//cdn.shopify.com/s/files/1/0032/3279/2674/t/60/assets/slick.js?8588643287693252372" type="text/javascript"></script>
<script>
    var $slider = $(".home_slider");
    $slider.
    on('init', function () {
        mouseWheel($slider);
    }).
    slick({
        dots: false,
        arrows: false,
        nav: false,
        vertical: true,
        infinite: false });

    function mouseWheel($slider) {
        $(window).on('wheel', { $slider: $slider }, mouseWheelHandler);
    }
    function mouseWheelHandler(event) {
        event.preventDefault();
        var $slider = event.data.$slider;
        var delta = event.originalEvent.deltaY;
        if (delta > 0) {
            $slider.slick('slickNext');
        } else
        {
            $slider.slick('slickPrev');
        }
    }

    $(function(){
        $('html').keydown(function(e){
            $('.home_slider').slick('slickNext');
        });
    });

    $('.home_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){

        var _each1 = $(this).next().find('.subtitle-frame').find('.scroll-section');
        var _each2 = $(this).next().find('.title-frame').find('.scroll-section');
        var _each3 = $(this).next().find('.copy-frame').find('.scroll-section');
        if(nextSlide == '0'){
            $(_each1).css('transform', 'translateY(0%)');
            $(_each2).css('transform', 'translateY(0%)');
            $(_each3).css('transform', 'translateY(0%)');
        }else if(nextSlide == '1'){
            $(_each1).css('transform', 'translateY(-100%)');
            $(_each2).css('transform', 'translateY(-100%)');
            $(_each3).css('transform', 'translateY(-100%)');
        }else if(nextSlide == '2'){
            $(_each1).css('transform', 'translateY(-200%)');
            $(_each2).css('transform', 'translateY(-200%)');
            $(_each3).css('transform', 'translateY(-200%)');
        }else if(nextSlide == '3'){
            $(_each1).css('transform', 'translateY(-300%)');
            $(_each2).css('transform', 'translateY(-300%)');
            $(_each3).css('transform', 'translateY(-300%)');
        }
    });

</script>
</body>
</html>
