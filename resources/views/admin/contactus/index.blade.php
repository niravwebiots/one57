@extends('layouts.admin')

@section('title') Admin | Contact @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('back/css/datatable-extension.css') }}" />
<style>
    .card .card-body form, .card .card-body form > a, .card .card-body form > button{display:inline-flex;}
    .card .card-body form > a.btn-success{margin-right: 10px;}
    .card .card-body form .icofont{line-height: 24px; padding-right: 8px;}
</style>  
</v-system-bar>

@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Contact</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">Contact</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Contact</h5>
                    <div class="card-header-right">
                        <div class="btn btn-primary" onclick="openModal()" >Contact</div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="contactus" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Customer Name</th>
                                    <th>email</th>
                                    <th>contactno</th>
                                    <th>address</th>
                                    <th>city</th>
                                    <th>state</th>
                                    <th>country</th>
                                    <th>zip</th>
                                    <th>home size</th>
                                    <th>broker</th>
                                    <th>hearaboutus</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($contactus as $c)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $c->customer_name }}</td>
                                    <td>{{ $c->email }}</td>
                                    <td>{{ $c->contactno }}</td>
                                    <td>{{ $c->address }}</td>
                                    <td>{{ $c->city }}</td>
                                    <td>{{ $c->state }}</td>
                                    <td>{{ $c->countries->country_name }}</td>
                                    <td>{{ $c->zip }}</td>
                                    <td>{{ $c->homesize->size_name }}</td>
                                    <td>{{ $c->brokers->broker_name }}</td>
                                    <td>{{ $c->hearabout->reference_name }}</td>
                                    <td>
                                        <form action="" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{method_field('DELETE')}}

                                            <a href="" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form method="post" action="{{ route('admin.set.contact') }}">
        {{ csrf_field() }}
        <input type="hidden" name="type" id="type" value="">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Contact Page Description</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div width="25%">
                <textarea class="form-control" id="contact_description" name="contact_description" rows="10"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </div>
    </form>

  </div>
</div>

@endsection

@section('script')

<!--Extension table js-->
<script src="{{ asset('back/js/datatable-extension/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('back/js/datatable-extension/jszip.min.js') }}"></script>
<script src="{{ asset('back/js/datatable-extension/buttons.print.min.js') }}"></script>
<script src="{{ asset('back/js/datatable-extension/buttons.html5.min.js') }}"></script>

<script type="text/javascript">
    function openModal()
    {
        var typ = 1;

        $.ajax({
            url:"{{url('get-option')}}/"+typ,
            type:'GET',
            async: false,
            success: function(result) {

                var contactus = jQuery.parseJSON(result);
                
                $('#type').val(typ);
                $('#contact_description').text(contactus.description);

                $('#myModal').modal();
            },
            error: function(result) {
                alert('error');
            }
        });
    }
</script>

<script type="text/javascript">

    $('#contactus').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                text:'Export Excel',
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                }
            }
        ]
    } );

</script>
@endsection