@extends('layouts.admin')

@section('title') Admin | News @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>News</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">News</li>
                    <li class="breadcrumb-item active">Add News</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add News</h5>
                </div>
                <form action="{{ route('news.store') }}" class="form theme-form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Magazine Name</label>
                                <input type="text" class="form-control" name="magazine_name" value="{{ old('magazine_name') }}">
                                <span class="text-danger">{{ $errors->first('magazine_name') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Magazine Image</label>
                                <input type="file" class="form-control" name="magazine_image" id="magazine_image">
                                <span class="text-danger">{{ $errors->first('magazine_image') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <img src="" id="magazine_image_prev" style="width: 20%;">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>News Date</label>
                                <input type="text" class="form-control" name="news_date" value="{{ old('news_date') }}">
                                <span class="text-danger">{{ $errors->first('news_date') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>News Image</label>
                                <input type="file" class="form-control" name="news_image" id="news_image">
                                <span class="text-danger">{{ $errors->first('news_image') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <img src="" id="news_image_prev" style="width: 40%;">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>News Title</label>
                                <input type="text" class="form-control" name="news_title" value="{{ old('news_title') }}">
                                <span class="text-danger">{{ $errors->first('news_title') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>News Link</label>
                                <input type="text" class="form-control" name="news_link" value="{{ old('news_link') }}">
                                <span class="text-danger">{{ $errors->first('news_link') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Medium</label>
                                <select name="medium" class="form-control">
                                    <option value="">select</option>
                                    <option value="0">Digital</option>
                                    <option value="1">Printed</option>
                                </select>
                                <span class="text-danger">{{ $errors->first('medium') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>News Description</label>
                                <textarea class="form-control" id="description" name="news_description">{{ old('news_description') }}</textarea>
                                <span class="text-danger">{{ $errors->first('news_description') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('news.index') }}" class="btn btn-light">Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<!--ckeditor js-->
<script src="{{ asset('back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/styles.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description', {
        on: {
            contentDom: function( evt ) {
                // Allow custom context menu only with table elemnts.
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();

                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    } );
</script>

<script type="text/javascript">
    function magazine_image(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#magazine_image_prev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#magazine_image").change(function() {
      magazine_image(this);
    });

    function news_image(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#news_image_prev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#news_image").change(function() {
      news_image(this);
    });
</script>
@endsection