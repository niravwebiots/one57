@extends('layouts.admin')

@section('title') Admin | Welcome @endsection

@section('style')
<style>
    .morecontent span {
        display: none;
    }
    .morelink {
        display: block;
    }
</style>
@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>welcome</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">welcome</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>welcome</h5>
                    
                    <div class="card-header-right">
                        <a href="{{ route('welcome.create') }}" class="btn btn-primary" >Add welcome</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="welcome" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Headline</th>
                                    <th>Introcontent</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($welcome as $w)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $w->headline }}</td>
                                    <td class="more" width="40%">{!! $w->introcontent !!}</td>
                                    <td>
                                        @if($w->status == 0)
                                            <a href="{{ route('admin.welcome.status',[$w->id,'1']) }}" class="txt-danger">Deactive</a>
                                        @elseif($w->status == 1)
                                            <a href="{{ route('admin.welcome.status',[$w->id,'0']) }}" class="txt-success">Active</a>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('welcome.destroy',$w->id) }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{method_field('DELETE')}}

                                            <a href="{{ route('welcome.edit',$w->id) }}" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <!-- <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button> -->
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<script type="text/javascript">

    $('#welcome').DataTable();


    $(document).ready(function() {
        var showChar = 100;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more').each(function() {
            var content = $(this).html();
            $('.morecontent > span').hide();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);

                var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });

</script>
@endsection