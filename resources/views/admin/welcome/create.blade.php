@extends('layouts.admin')

@section('title') Admin | Welcome @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Welcome</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">Welcome</li>
                    <li class="breadcrumb-item active">Add Welcome</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Welcome</h5>
                </div>
                <form action="{{ route('welcome.store') }}" class="form theme-form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Welcome Headline</label>
                                <input type="text" class="form-control" name="headline" value="{{ old('headline') }}">
                                <span class="text-danger">{{ $errors->first('headline') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Welcome Video Link</label>
                                <input type="text" class="form-control" name="background_url" value="{{ old('background_url') }}">
                                <span class="text-danger">{{ $errors->first('background_url') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Welcome Button Title</label>
                                <input type="text" class="form-control" name="button_title" value="{{ old('button_title') }}">
                                <span class="text-danger">{{ $errors->first('button_title') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Welcome Link</label>
                                <input type="text" class="form-control" name="link" value="{{ old('link') }}">
                                <span class="text-danger">{{ $errors->first('link') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Introcontent</label>
                                <textarea class="form-control" id="introcontent" name="introcontent">{{ old('introcontent') }}</textarea>
                                <span class="text-danger">{{ $errors->first('introcontent') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('welcome.index') }}" class="btn btn-light">Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<!--ckeditor js-->
<script src="{{ asset('back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/styles.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'introcontent', {
        on: {
            contentDom: function( evt ) {
                // Allow custom context menu only with table elemnts.
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();

                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    } );
</script>
@endsection