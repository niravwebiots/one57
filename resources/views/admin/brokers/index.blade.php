@extends('layouts.admin')

@section('title') Admin | Brokers @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Brokers</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">Brokers</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Brokers</h5>
                    
                    <div class="card-header-right">
                        <a href="{{ route('brokers.create') }}" class="btn btn-primary" >Add Broker</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="brokers" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Brokers Questions</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($brokers as $h)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $h->broker_name }}</td>
                                    <td>
                                        @if($h->status == 0)
                                            <a href="{{ route('admin.brokers.status',[$h->id,'1']) }}" class="txt-danger">Deactive</a>
                                        @elseif($h->status == 1)
                                            <a href="{{ route('admin.brokers.status',[$h->id,'0']) }}" class="txt-success">Active</a>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('brokers.destroy',$h->id) }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{method_field('DELETE')}}

                                            <a href="{{ route('brokers.edit',$h->id) }}" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<script type="text/javascript">

    $('#brokers').DataTable();

</script>
@endsection