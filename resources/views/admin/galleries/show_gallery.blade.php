@extends('layouts.admin')

@section('title') Admin | Galleries @endsection

@section('style')

<style>
.switch-field input {
    position: absolute !important;
    clip: rect(0, 0, 0, 0);
    height: 1px;
    width: 1px;
    border: 0;
    overflow: hidden;
}

.switch-field label {
  float: left;
}

.switch-field label {
  display: inline-block;
  width: 90px;
  background-color: #e4e4e4;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
  font-weight: normal;
  text-align: center;
  text-shadow: none;
  padding: 6px 14px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  -webkit-transition: all 0.1s ease-in-out;
  -moz-transition:    all 0.1s ease-in-out;
  -ms-transition:     all 0.1s ease-in-out;
  -o-transition:      all 0.1s ease-in-out;
  transition:         all 0.1s ease-in-out;
}

.switch-field label:hover {
    cursor: pointer;
}

.switch-field input:checked + label {
  background-color: #ab8ce4;
  -webkit-box-shadow: none;
  box-shadow: none;
  color: #ffffff;
}

.switch-field label:first-of-type {
  border-radius: 4px 0 0 4px;
}

.switch-field label:last-of-type {
  border-radius: 0 4px 4px 0;
}

</style>

@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Galleries</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">Galleries</li>
                    <li class="breadcrumb-item active">Edit Gallery</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Gallery</h5>
                </div>
                <form action="{{ route('galleries.update', $gallery->id) }}" class="form theme-form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{method_field('PUT')}}

                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Gallery Title</label>
                                <input type="text" class="form-control" name="title" value="{{ $gallery->title }}">
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Gallery Category</label>
                                <select class="form-control" name="gallery_category">
                                    <option value="">Select</option>
                                    <option value="0" @if($gallery->category == 0 ) selected @endif>Gallery</option>
                                    <option value="1" @if($gallery->category == 1 ) selected @endif>Overseas</option>
                                    <option value="2" @if($gallery->category == 2 ) selected @endif>Constration Status</option>
                                </select>
                                <span class="text-danger">{{ $errors->first('gallery_category') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Gallery Type</label>
                                <div class="switch-field">
                                  <input type="radio" id="switch_left" class="type" name="gallery_type" value="image" @if($gallery->image != "") checked @endif/>
                                  <label for="switch_left">Image</label>
                                  <input type="radio" id="switch_right" class="type" name="gallery_type" value="video" @if($gallery->video_url != "") checked @endif />
                                  <label for="switch_right">Video</label>
                                </div>
                                <span class="text-danger">{{ $errors->first('gallery_type') }}</span>
                            </div>
                        </div>                        

                        <div class="row input_type" id="image" @if($gallery->image == "") style="display: none;" @endif>
                            <div class="form-group col-md-4">
                                <label>Gallery Image</label>
                                <input type="file" class="form-control" name="image" id="imgupload" >
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                @if($gallery->image == "")
                                <img src="" id="imageprev" style="width: 80%;">
                                @else
                                <img src="{{ url('uploads/galleries') }}/{{$gallery->image}}" id="imageprev" style="width: 80%;">
                                @endif
                            </div>
                        </div>

                        <div class="row input_type" id="video" @if($gallery->video_url == "") style="display: none;" @endif>
                            <div class="form-group col-md-4">
                                <label>Gallery Video</label>
                                <input type="text" class="form-control" name="video_url" id="videoupload" value="{{ $gallery->video_url }}">
                                <span class="text-danger">{{ $errors->first('video_url') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('galleries.index') }}" class="btn btn-light">Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#imageprev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgupload").change(function() {
      readURL(this);
    });

    $(function() {
      $('.type').change(function(){
        $('.input_type').hide();
        $('#' + $(this).val()).show();

        var data = $(this).val();

        if(data == 'image')
        {
            $( "#videoupload" ).val('');
        }
        else if(data == 'video')
        {
            $('#imgupload').val('');
        }

      });
    });
</script>
@endsection