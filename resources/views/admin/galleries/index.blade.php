@extends('layouts.admin')

@section('title') Admin | Galleries @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Galleries</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">Galleries</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Galleries</h5>
                    
                    <div class="card-header-right">
                        <a href="{{ route('galleries.create') }}" class="btn btn-primary" >Add New Gallery</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="galleries" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($galleries as $g)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>
                                        @if($g->image != "")
                                            <img src="{{ url('uploads/galleries') }}/{{ $g->image }}" style="width: 100px;">
                                        @elseif($g->video_url != "")
                                            {{ $g->video_url }}
                                        @endif
                                    </td>
                                    <td>{{ $g->title }}</td>
                                    <td>
                                        @if($g->status == 0)
                                            <a href="{{ route('admin.galleries.status',[$g->id,'1']) }}" class="txt-danger">Deactive</a>
                                        @elseif($g->status == 1)
                                            <a href="{{ route('admin.galleries.status',[$g->id,'0']) }}" class="txt-success">Active</a>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('galleries.destroy',$g->id) }}" method="POST">
                                            {{method_field('DELETE')}}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <a href="{{ route('galleries.edit',$g->id) }}" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<script type="text/javascript">

    $('#galleries').DataTable();

</script>
@endsection