@extends('layouts.admin')

@section('title') Admin | Home Slider @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('back/css/datatable-extension.css') }}" />
<style>
    .morecontent span {
        display: none;
    }
    .morelink {
        display: block;
    }
</style>
@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Home Slider</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">Home Slider</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Home Slider</h5>
                    
                    <div class="card-header-right">
                        <!-- <a href="{{ route('homeslider.create') }}" class="btn btn-primary" >Add Sliders</a> -->
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="sliders" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($sliders as $s)
                                <tr id="{{$s->id}}">
                                    <td>{{ $i }}</td>
                                    <td><img src="{{ url('uploads/homeslider') }}/{{ $s->background_image }}" style="width: 100px;"></td>
                                    <td>{{ $s->title }}</td>
                                    <td>{{ $s->subtitle }}</td>
                                    <td width="30%" class="more">{!! $s->description !!}</td>
                                    <td>
                                        @if($s->status == 0)
                                            <a href="{{ route('admin.homeslider.status',[$s->id,'1']) }}" class="txt-danger">Deactive</a>
                                        @elseif($s->status == 1)
                                            <a href="{{ route('admin.homeslider.status',[$s->id,'0']) }}" class="txt-success">Active</a>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('homeslider.destroy',$s->id) }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{method_field('DELETE')}}

                                            <a href="{{ route('homeslider.edit',$s->id) }}" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<!--datatable extension js-->
<script src="{{ asset('back/js/dataTables.rowReorder.min.js') }}"></script>

<script type="text/javascript">

    var table = $('#sliders').DataTable( {
        rowReorder: true
    });

    table.on( 'row-reorder', function ( e, diff, edit ) {

        var newPosition = new Array();

        newPosition = edit.values;

        $.ajax({
            url: '{{ route("admin.homeslider.set.position") }}',
            type: 'POST',
            data: { "_token": "{{ csrf_token() }}","newPosition":newPosition },
            success: function (s) {
                console.log(s)
            }
        });

    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        var showChar = 100;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more p').each(function() {
            var content = $(this).html();
            $('.morecontent > span').hide();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);

                var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>
@endsection