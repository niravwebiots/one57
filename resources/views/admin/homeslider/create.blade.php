@extends('layouts.admin')

@section('title') Admin | Home Slider @endsection

@section('style')
    <style>
        input[type="color"] {
            padding: 0;
            border: none;
            height: 50px;
            width: 50px;
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Home Slider</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">Home Slider</li>
                    <li class="breadcrumb-item active">Add Slider</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Slider</h5>
                </div>
                <form action="{{ route('homeslider.store') }}" class="form theme-form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Slider Title</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Slider Sub Title</label>
                                <input type="text" class="form-control" name="subtitle" value="{{ old('subtitle') }}">
                                <span class="text-danger">{{ $errors->first('subtitle') }}</span>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Slider Link</label>
                                <input type="text" class="form-control" name="link" value="{{ old('link') }}">
                                <span class="text-danger">{{ $errors->first('link') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Slider Image</label>
                                <input type="file" class="form-control" name="background_image" id="background_image">
                                <span class="text-danger">Note: Please select image width=1110 and height=443</span>
                                <span class="text-danger">{{ $errors->first('background_image') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <img src="" id="slider_image_prev" style="width: 50%;">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Select Font Color picker</label>
                                <input class="form-control" type="color" name="font_color" value="#ffffff">
                                <span class="text-danger">{{ $errors->first('font_color') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Select Background Color picker</label>
                                <input class="form-control" type="color" name="background_color" value="#ffffff">
                                <span class="text-danger">{{ $errors->first('background_color') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Slider Description</label>
                                <textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="form-group col-md-2">
                                <a href="{{ route('homeslider.index') }}" class="btn btn-light">Cancel</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<!--ckeditor js-->
<script src="{{ asset('back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/styles.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description', {
        on: {
            contentDom: function( evt ) {
                // Allow custom context menu only with table elemnts.
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();

                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    } );
</script>

<script type="text/javascript">
    function background_image(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#slider_image_prev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#background_image").change(function() {
      background_image(this);
    });
</script>
@endsection