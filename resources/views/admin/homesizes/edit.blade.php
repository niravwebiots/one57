@extends('layouts.admin')

@section('title') Admin | Edit Homesize @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>Homesizes</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">Homesizes</li>
                    <li class="breadcrumb-item active">Edit Homesize</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Homesize</h5>
                </div>
                <form action="{{ route('homesizes.update',$homesize->id) }}" class="form theme-form" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{method_field('PUT')}}

                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Homesize Name</label>
                                <input type="text" class="form-control" name="homesize_name" value="{{ old('homesize_name') ? old('homesize_name') : $homesize->size_name }}">
                                <span class="text-danger">{{ $errors->first('homesize_name') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('homesizes.index') }}" class="btn btn-light">Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection