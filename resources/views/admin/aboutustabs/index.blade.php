@extends('layouts.admin')

@section('title') Admin | About Us Tabs @endsection

@section('style')
<style>
    .morecontent span {
        display: none;
    }
    .morelink {
        display: block;
    }
</style>
@endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>About Us Tabs</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">About Us Tabs</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>About Us Tabs</h5>
                    
                    <div class="card-header-right">
                        <a href="{{ route('aboutustabs.create') }}" class="btn btn-primary" >Add Tab</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="aboutus" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp

                                @foreach($aboutus as $ab)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $ab->main_title }}</td>
                                    <td>{{ $ab->sub_title }}</td>
                                    <td width="30%" class="more">{!! $ab->description !!}</td>
                                    <td>
                                        @if($ab->status == 0)
                                            <a href="{{ route('admin.aboutustabs.status',[$ab->id,'1']) }}" class="txt-danger">Deactive</a>
                                        @elseif($ab->status == 1)
                                            <a href="{{ route('admin.aboutustabs.status',[$ab->id,'0']) }}" class="txt-success">Active</a>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('aboutustabs.destroy',$ab->id) }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{method_field('DELETE')}}

                                            <a href="{{ route('aboutustabs.edit',$ab->id) }}" class="btn btn-success">
                                                <i class="icofont icofont-pencil-alt-5"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger">
                                                <i class="icofont icofont-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<script type="text/javascript">
    $('#aboutus').DataTable();
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var showChar = 100;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more p').each(function() {
            var content = $(this).html();
            $('.morecontent > span').hide();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);

                var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>
@endsection