@extends('layouts.admin')

@section('title') Admin | Add Tab @endsection

@section('content')
<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6">
                <h3>About Us Tabs</h3>
            </div>
            <div class="col-lg-6">
                <ol class="breadcrumb pull-right">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item">About Us Tabs</li>
                    <li class="breadcrumb-item active">Add Tab</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends -->

<!-- Container-fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Tab</h5>
                </div>
                <form action="{{ route('aboutustabs.store') }}" class="form theme-form" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Tab Main Title</label>
                                <input type="text" class="form-control" name="main_title" value="{{ old('main_title') }}">
                                <span class="text-danger">{{ $errors->first('main_title') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Tab Sub Title</label>
                                <input type="text" class="form-control" name="sub_title" value="{{ old('sub_title') }}">
                                <span class="text-danger">{{ $errors->first('sub_title') }}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Tab Description</label>
                                <textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('aboutustabs.index') }}" class="btn btn-light">Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts -->
@endsection

@section('script')

<!--ckeditor js-->
<script src="{{ asset('back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/styles.js') }}"></script>
<script src="{{ asset('back/js/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description', {
        on: {
            contentDom: function( evt ) {
                // Allow custom context menu only with table elemnts.
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();

                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    } );
</script>
@endsection