<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Contact Us</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/fullpage.js/dist/fullpage.min.css">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>
<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>
<section>
    <div class="contact_bg_img">
        <div class="form_wrapper">
            <div class="form_container">
                <div class="row clearfix form_width_set">
                    <div class="">
                        <form>
                            <div class="input_field">
                                <input type="text" name="name" placeholder="NAME"  />
                            </div>
                            <div class="input_field">
                                <input type="email" name="email" placeholder="EMAIL" />
                            </div>
                            <div class="input_field">
                                <input type="text" name="phone" placeholder="PHONE"  />
                            </div>
                            <div class="input_field">
                                <input type="text" name="add" placeholder="ADDRESS"  />
                            </div>
                            <div class="input_field">
                                <select>
                                    <option value="">COUNTRY</option>
                                    <option value="">UNITED STATE</option>
                                    <option value="">AFGHANISTAN</option>
                                    <option value="">ALGERIA</option>
                                </select>
                            </div>
                            <div class="input_field">
                                <input type="text" name="city" placeholder="CITY"  />
                            </div>
                            <div class="row clearfix">
                                <div class="col-6 pr_7">
                                    <div class="input_field">
                                        <input type="text" name="state" placeholder="STATE" class="half-column1"/>
                                    </div>
                                </div>
                                <div class="col-6 pl_7">
                                    <div class="input_field">
                                        <input type="text" name="zip" placeholder="ZIP"  class="half-column2" />
                                    </div>
                                </div>
                            </div>
                            <div class="input_field">
                                <select>
                                    <option value="">PREFERRED HOME SIZE?</option>
                                    <option value="">1-BEDROOM</option>
                                    <option value="">2-BEDROOM</option>
                                    <option value="">3-BEDROOM</option>
                                    <option value="">4-BEDROOM</option>
                                </select>
                            </div>
                            <div class="input_field">
                                <select>
                                    <option value="">ARE YOU A BROKER?</option>
                                    <option value="">YES, I AM A BROKER</option>
                                    <option value="">NO, I AM NOT A BROKER</option>
                                </select>
                            </div>
                            <div class="input_field">
                                <select>
                                    <option value="">HOW DID YOU HEAR ABOUT AD?</option>
                                    <option value="">PRESS</option>
                                    <option value="">ADVERTISEMENT</option>
                                    <option value="">BROKER</option>
                                </select>
                            </div>
                            <input class="button btn-set" type="submit" value="Register" />
                        </form>
                    </div>
                </div>
                <div class="title_container bottom-section">
                    <p>Please call our on-site sales center at 212-570-1700 or Email sales@one57.com</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</body>
</html>
