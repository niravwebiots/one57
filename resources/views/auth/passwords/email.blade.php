<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/fontawesome.css') }}">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/icofont.css') }}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/themify.css') }}">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/flag-icon.css') }}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/bootstrap.css') }}">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/style.css') }}">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('back/css/responsive.css') }}">

</head>

<body>

<!-- Loader starts -->
<!-- <div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <h4>Have a great day at work today <span>&#x263A;</span></h4>
    </div>
</div> -->
<!-- Loader ends -->

<!--page-wrapper Start-->
<div class="page-wrapper">

    <div class="container-fluid">

        <!--Reset Password page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-4 p-0">
                    <div class="auth-innerleft">
                        <div class="text-center">
                            <img src="{{ asset('back/images/key.png') }}" class="img-fluid security-icon" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-8 p-0">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <h3>RESET YOUR PASSWORD</h3>

                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="card mt-4 p-4">
                                <form method="post" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}" class="theme-form">
                                    @csrf
                                    <h5 class="f-16 mb-3">Reset Password</h5>
                                    <div class="form-group">
                                        <label for="email" class="col-form-label">E-Mail Address</label>
                                        <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-row mb-0">
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-secondary">
                                                {{ __('Send Password Reset Link') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Reset Password page end-->
    </div>

</div>
<!--page-wrapper Ends-->

<!-- latest jquery-->
<script src="{{ asset('back/js/jquery-3.2.1.min.js') }}"></script>

<!-- Bootstrap js-->
<script src="{{ asset('back/js/popper.min.js') }}"></script>
<script src="{{ asset('back/js/bootstrap.js') }}"></script>

<!-- Theme js-->
<script src="{{ asset('back/js/script.js') }}" ></script>

</body>
</html>