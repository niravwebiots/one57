<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body>


<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>

<div id="wall">

    <!-- home section -->
    <section class="section-1 text-center p-c-c">
        <div class="inner-part">
            <video playsinline="playsinline" autoplay="autoplay" class="slider-video" muted="muted" loop="loop">
                <source src="http://realestate.webiots.co.in/uploads/welcome/videos.mp4" type="video/mp4">
            </video>
        </div>
        <div class="img-content">
            <div>
                <h1>Park Hyatt</h1>
                <p>Park Hyatt Hotel marks the arrival of service with a pedigree previously unavailable in New York. International&nbsp;travelers&nbsp;are among those most accustomed to the philosophy of gracious service that informs Park Hyatt's ethos:&nbsp;luxury is personal. Park Hyatt New York was named the No.1 hotel in New York by U.S. News and World Report.</p>
                <div class="cities-list"><p>New York / Washington / Chicago / Vienna / Milan / Paris / Zurich / Moscow / Istanbul / Beijing / Shanghai / Tokyo / Saigon / Zanzibar / Dubai / Sydney / Bangkok / Seoul</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div>
            <div data-wall-slide>
                <div class="img-section">
                    <img src="{{ asset('front/images/slider_3.jpg')}}" class="img-fluid" alt="">
                </div>
            </div>
            <div data-wall-slide>
                <div class="img-section">
                    <img src="{{ asset('front/images/slider_2.jpg')}}" class="img-fluid" alt="">
                </div>
            </div>
            <div data-wall-slide-arrow class="prev-slide"></div>
            <div data-wall-slide-arrow class="next-slide"></div>
        </div>
    </section>
    <section>
        <div class="img-section">
            <img src="{{ asset('front/images/slider_1.jpg')}}" class="img-fluid" alt="">
        </div>
    </section>
<!--    <section>-->
<!--        <div class="container-fluid p-0">-->
<!--            <div class="row">-->
<!--                <div class="col-6 p-0">-->
<!--                    <div class="img-section">-->
<!--                        <img src="{{ asset('front/images/1.jpg')}}" class="img-fluid" alt="">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-6 p-0">-->
<!--                    <div class="img-section">-->
<!--                        <img src="{{ asset('front/images/2.jpg')}}" class="img-fluid" alt="">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>living room</li>
        <li>bevy</li>
        <li>residence service</li>
    </ul>
</div>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>

<script>

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>

</body>
</html>
