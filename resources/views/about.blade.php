<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>

<body>


<nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top about_as_black" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="#"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_aboutus_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>

<div id="wall">
    <section>
        <div class="img-section">
            <div class="container">
                <div class="about_first_slider_content">
                    <div class="about_content">
                        <h1>About The Crown</h1>
                        <h2>About Astoria</h2>
                        <div class="about_card">
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online.</p>
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                            <p>Here at Techmagnate, we live and breathe digital content marketing. With expert editors and a talented writing staff on our fingertips, we work with each and every client to develop and devise a unique approach to their industry, applying a set principle of key steps to create a fleshed-out, comprehensive content plan.</p>
                            <p>Here at Techmagnate, we live and breathe digital content marketing. With expert editors and a talented writing staff on our fingertips, we work with each and every client to develop and devise a unique approach to their industry, applying a set principle of key steps to create a fleshed-out, comprehensive content plan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="img-section">
            <div class="container">
                <div class="about_first_slider_content">
                    <div class="about_content">
                        <h1>About Risland</h1>
                        <div class="about_card">
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online.</p>
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                            <p>Here at Techmagnate, we live and breathe digital content marketing. With expert editors and a talented writing staff on our fingertips, we work with each and every client to develop and devise a unique approach to their industry, applying a set principle of key steps to create a fleshed-out, comprehensive content plan. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                            <p>Here at Techmagnate, we live and breathe digital content marketing. With expert editors and a talented writing staff on our fingertips, we work with each and every client to develop and devise a unique approach to their industry, applying a set principle of key steps to create a fleshed-out, comprehensive content plan.  Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="img-section">
            <div class="slider_map">
                <div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7439.304714596439!2d72.83103573797037!3d21.205964806178933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f3ef077dd21%3A0xb15b8efb5a08fa00!2sMango+Education+%26+Immigration!5e0!3m2!1sen!2sin!4v1546416295696" width="100%" height="580px" frameborder="0" allowfullscreen="allowfullscreen" style="border: 0px;"></iframe>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="img-section img_section1">
            <div class="container">
                <div class="aboutus_tabs_set">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">First Panel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Second Panel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Third Panel</a>
                        </li>
                    </ul><!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <p>Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                        </div>
                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <p>Here at Techmagnate, we live and breathe digital content marketing. With expert editors and a talented writing staff on our fingertips, we work with each and every client to develop and devise a unique approach to their industry, applying a set principle of key steps to create a fleshed-out, comprehensive content plan.  Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content. Content marketing is about a forward-thinking vision – as the Internet ages, information becomes the prevalent and prominent source of value online. It’s always been about content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>The Crown</li>
        <li>Risland</li>
        <li>Map</li>
        <li>Brand Associates</li>
    </ul>
</div>

<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>

<script>

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>

</body>
</html>
