<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Home</title>



    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/slick-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
</head>
<body >

<nav  class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top about_as_black" id="mainNav">
    <div class="container">
        <a class="navbar-brand navbar_brand_set" href="{{ route('slider') }}"><img src="{{ asset('front/images/ace-logo_set.png')}}" alt="" class="img-fluid" width="100px"></a>
        <a class="ml-auto mobile-enquiry" href="{{ route('contact') }}" >enquiry</a>
        <button class="navbar-toggler navabr_btn-set collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav navbar_nav_modify">
            @include('menubar')
            </ul>
        </div>
    </div>
</nav>
<div id="wall">
    <section  id ="2">
        <div class="img-section">
        <!--  <img src="{{ asset('front/images/slider_1.jpg')}}" class="img-fluid" alt=""> -->
            <div class='container container_news'>
                <div class='single-item'>
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Digital News</h2>
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;">
                                        <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section  id="3">
        <div class="img-section">
        <!--  <img src="{{ asset('front/images/slider_1.jpg')}}" class="img-fluid" alt=""> -->
            <div class='container container_news'>
                <div class='single-item'>
                    {{--<div class="news-item-wrapper">--}}
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Print News</h2>
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    {{--second slider--}}
                    <div class="row">
                        <div class="col-md-12">
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    {{--third slider--}}
                    <div class="row">
                        <div class="col-md-12">
                            <ul style="list-style-type: none;" class="news_ul_a_tag">
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>
                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                                <hr>

                                <a href="{{ route('single_news') }}" style="display: flex">
                                    <li style="width: 25%;"><div class="news-item-logo1"></div></li>
                                    <li style="width: 50%;"> <div class="news-item-content">
                                            <h3 class="news_h3">Spring Garden Residence with Ryan Serhant</h3>
                                            <h4 class="news-item-date">January 22, 2019</h4>
                                        </div>
                                    </li>
                                    <li style="width: 25%;"><div class="news-item-image"></div></li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    <div class="news_slider_content">
                        <p>For press inquiries, please contact: M18 Public Relations
                            Anna LaPorte +1 212-604-0318 or press@one57.com</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>
<div class="slider_nav">
    <ul class="nav" data-wall-section-nav>
        <li>Digital News</li>
        <li>Print News</li>
    </ul>
</div>
<script src='{{ asset("front/js/jquery.min.js") }}'></script>
<script src='{{ asset("front/js/popper.min.js") }}'></script>
<script src='{{ asset("front/js/bootstrap.min.js") }}'></script>
<script src='{{ asset("front/js/slick.min.js") }}'></script>
<script src='{{ asset("front/js/wall.js") }}'></script>

<script>
    $(".single-item").slick({
        dots: true
    });

    (function () {

        var wall = new Wall('#wall');
        console.log(wall);

        document.querySelector('.prev-slide').addEventListener('click', function () { wall.prevSlide(); });
        document.querySelector('.next-slide').addEventListener('click', function () { wall.nextSlide(); });
    }());

</script>

</body>
</html>
